
create table users (
    id int(11) primary key auto_increment,
    username varchar(50) not null unique ,
    password varchar(50) not null
);

create table students (
    sid int(11) primary key auto_increment,
    username varchar(50) not null unique,
    password varchar(50) not null,
    name varchar(50) not null
);

create table exams (
    eid int(11) primary key auto_increment,
    subject varchar(50) not null
);

create table questions (
    eid int(11) not null,
    qid int(11) primary key auto_increment,
    question text not null,
    foreign key (eid) references exams(eid)
);

create table answers (
    eid int(11) not null,
    qid int(11) not null,
    aid int(11) primary key auto_increment,
    content text,
    isCorrect boolean default false,
    foreign key(eid) references exams(eid),
    foreign key(qid) references questions(qid)
);

create table exam_results (
    rid int(11) primary key auto_increment,
    eid int(11) not null,
    sid int(11) not null,
    wront_count int,
    correct_count int,
    foreign key(eid) references exams(eid),
    foreign key(sid) references students(sid)
);