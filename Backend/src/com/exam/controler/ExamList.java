package com.exam.controler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.exam.dao.ExamDao;
import com.exam.dao.StudentDao;
import com.exam.models.Exam;
import com.exam.models.Student;
import com.exam.utils.Response;

/**
 * Servlet implementation class ExamList
 */
@WebServlet(name = "examlist", urlPatterns = { "/examlist" })
public class ExamList extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			int sid = Integer.parseInt(request.getParameter("sid"));
			StudentDao studentDao = new StudentDao();
			Student student = studentDao.getStudent(sid);
			if(student != null) {
				ExamDao examDao = new ExamDao();
				ArrayList<Exam> exams = examDao.getStudentNewExams(student);
				if(exams != null) {
					Response.responseBuilder(200, response, exams);
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "No exams for now");
					Response.responseBuilder(404, response, responseJson);
				}
			}else {
				responseJson.put("status", "error");
				responseJson.put("message", "Student does not found");
				Response.responseBuilder(404, response, responseJson);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
		
	}

}
