package com.exam.controler;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.exam.dao.AnswerDao;
import com.exam.dao.ExamDao;
import com.exam.dao.QuestionDao;
import com.exam.models.ExamResult;
import com.exam.utils.Response;

/**
 * Servlet implementation class ExamReport
 */
@WebServlet(name = "reports/exam", urlPatterns = { "/reports/exam" })
public class ExamReport extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, Object> responseJson = new HashMap<String, Object>();
		try {
			int sid = request.getParameter("sid") != null ? Integer.parseInt(request.getParameter("sid")) : 0;
			int eid = request.getParameter("eid") != null ? Integer.parseInt(request.getParameter("eid")) : 0;
			ExamDao examDao = new ExamDao();
			ArrayList<ExamResult> results = examDao.getExamResults(sid, eid, true);
			
			examDao.closeConnection();
			
			AnswerDao answerDao = new AnswerDao();
			QuestionDao qd = new QuestionDao();
			
			
			if(results != null) {
				if(sid !=0 || eid != 0) {
					for(int i = 0; i< results.size() - 1 ; i++) {
						double score = 
								Double
								.parseDouble(new DecimalFormat("##.#")
								.format(examDao.calculateStudentAnswers(results.get(i).getSid(), results.get(i).getExamId(), answerDao, qd)));
						results.get(i).setScore(score);
					}
				}
				
				answerDao.closeConnection();
				qd.closeConnection();
				
				Response.responseBuilder(200, response, results);
			}else {
				responseJson.put("status", "error");
				responseJson.put("message", "No results found");
				Response.responseBuilder(404, response, responseJson);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
	}

}
