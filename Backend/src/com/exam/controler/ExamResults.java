package com.exam.controler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.exam.dao.ExamDao;
import com.exam.dao.QuestionDao;
import com.exam.models.ExamAnswer;
import com.exam.models.ExamResult;
import com.exam.utils.Response;
import com.google.gson.Gson;

/**
 * Servlet implementation class ExamResults
 */
@WebServlet(name = "results", urlPatterns = { "/results" })
public class ExamResults extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			int sid = Integer.parseInt(request.getParameter("sid"));
			ExamDao examDao = new ExamDao();
			ArrayList<ExamResult> examResults = examDao.getExamResults(sid, 0, true);
			if(examResults != null) {
				Response.responseBuilder(200, response, examResults);
			}else {
				responseJson.put("status", "error");
				responseJson.put("message", "No results found");
				Response.responseBuilder(404, response, responseJson);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			int examId = Integer.parseInt(request.getParameter("eid"));
			int sid = Integer.parseInt(request.getParameter("sid"));
			String jsonBody = new BufferedReader(new InputStreamReader(request.getInputStream())).lines().collect(
		            Collectors.joining("\n"));
			ExamDao examDao = new ExamDao();
			
			if(examDao.getExam(examId) != null) {
				Gson json = new Gson();
				ExamAnswer[] examResult = json.fromJson(jsonBody, ExamAnswer[].class);
				
				Map<Integer, Double> examAnswers = new HashMap<>();
				
				QuestionDao questionDao = new QuestionDao();
				
				double examMark = 0.0;
						
				for(ExamAnswer result: examResult) {
					int qid = result.getQuestionId();
					int[] answerIds = result.getAnswer();
					double questionScore = questionDao.matchCorrectAnswer(qid, answerIds);
					examAnswers.put(qid, questionScore);
					examMark += questionScore;
					
					for (int aid : answerIds) {
						examDao.saveQuestionResult(examId, qid, sid, aid);
					}
				}
				
				boolean examSubmited = examDao.submitExamResult(examId, sid, examMark);
				
				if(examSubmited) {
					responseJson.put("status", "success");
					responseJson.put("message", "Exam result submited success");
					Response.responseBuilder(201, response, responseJson);
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "There is an error");
					Response.responseBuilder(400, response, responseJson);
				}
				
			}else {
				responseJson.put("status", "error");
				responseJson.put("message", "No exam found");
				Response.responseBuilder(404, response, responseJson);
			}
			examDao.closeConnection();
		}catch(Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
	}

}
