package com.exam.controler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.exam.dao.ExamDao;
import com.exam.models.ExamSpec;
import com.exam.utils.Response;


@WebServlet(name = "specs", urlPatterns = { "/specs" })
public class ExamSpecs extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5755743123234551256L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			int id = request.getParameter("id") != null ? Integer.parseInt(request.getParameter("id")) : 0;
			int onlyActive = request.getParameter("only_active") != null ? Integer.parseInt(request.getParameter("only_active")) : 0;
			ExamDao examDao = new ExamDao();
			if(id == 0) {
				ArrayList<ExamSpec> specs = null;
				
				if(onlyActive == 0) {
					specs = examDao.getExamSpecs(false);
				}else {
					specs = examDao.getExamSpecs(true);
				}
				
				if(specs != null) {
					Response.responseBuilder(200, response, specs);
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "No specs for now");
					Response.responseBuilder(404, response, responseJson);
				}
			}else {
				ExamSpec spec = examDao.getSpec(id);
				if(spec != null) {
					Response.responseBuilder(200, response, spec);
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "No Specialization found");
					Response.responseBuilder(404, response, responseJson);
				}
			}
			
			examDao.closeConnection();
		} catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		} 
		
		
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			String specName = request.getParameter("spec_name").trim();
			boolean isActive = Boolean.valueOf(request.getParameter("is_active"));
			if(specName.length() > 0) {
				ExamDao examDao = new ExamDao();
				ExamSpec spec = new ExamSpec();
				spec.setSpec_name(specName);
				spec.setActive(isActive);
				if(examDao.addSpec(spec)) {
					responseJson.put("status", "success");
					responseJson.put("message", "Specialization created successfully");
					Response.responseBuilder(201, response, responseJson);
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "There is an error");
					Response.responseBuilder(400, response, responseJson);
				}
				examDao.closeConnection();
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
	}

	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			String specName = request.getParameter("spec_name").trim();
			boolean isActive = Boolean.valueOf(request.getParameter("is_active"));
			int id = Integer.parseInt(request.getParameter("id"));
			if(specName.length() > 0) {
				ExamDao examDao = new ExamDao();
				ExamSpec spec = new ExamSpec();
				spec.setSpec_id(id);
				spec.setSpec_name(specName);
				spec.setActive(isActive);
				if(examDao.updateSpec(spec)) {
					responseJson.put("status", "success");
					responseJson.put("message", "Specialization updated successfully");
					Response.responseBuilder(200, response, responseJson);
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "No Specialization found");
					Response.responseBuilder(404, response, responseJson);
				}
				examDao.closeConnection();
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
	}
}
