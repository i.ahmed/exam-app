package com.exam.controler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.exam.dao.ExamDao;
import com.exam.dao.QuestionDao;
import com.exam.models.Exam;
import com.exam.utils.Response;

/**
 * Servlet implementation class Exams
 */
@WebServlet(name = "exams", urlPatterns = { "/exams" })
public class Exams extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			int id = request.getParameter("id") != null ? Integer.parseInt(request.getParameter("id")) : 0;
			ExamDao examDao = new ExamDao();
			if(id == 0 ) {
				ArrayList<Exam> exams = examDao.getExams();
				Response.responseBuilder(200, response, exams);
			}else {
				Exam exam = examDao.getExam(id);
				if(exam != null) {
					Response.responseBuilder(200, response, exam);
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "No exam found");
					Response.responseBuilder(404, response, responseJson);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", "There is an error");
			Response.responseBuilder(400, response, responseJson);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			String subject = request.getParameter("subject").trim();
			int spec_id = request.getParameter("spec_id") != null ? Integer.parseInt(request.getParameter("spec_id")) : 0;
			if(subject.length() > 0 && spec_id != 0) {
				ExamDao examDao = new ExamDao();
				Exam exam = new Exam();
				exam.setSubject(subject);
				exam.setSpec_id(spec_id);
				if(examDao.addExam(exam)) {
					responseJson.put("status", "success");
					responseJson.put("message", "Exam created successfully");
					Response.responseBuilder(201, response, responseJson);
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "There is an error");
					Response.responseBuilder(400, response, responseJson);
				}
				examDao.closeConnection();
			}else {
				responseJson.put("status", "error");
				responseJson.put("message", "There is an error");
				Response.responseBuilder(400, response, responseJson);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
	}

	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			String subject = request.getParameter("subject").trim();
			int spec_id = request.getParameter("spec_id") != null ? Integer.parseInt(request.getParameter("spec_id")) : 0;
			int id = Integer.parseInt(request.getParameter("id"));
			if(subject.length() > 0 && spec_id != 0) {
				ExamDao examDao = new ExamDao();
				Exam exam = new Exam();
				exam.setEid(id);
				exam.setSpec_id(spec_id);
				exam.setSubject(subject);
				if(examDao.updateExam(exam)) {
					responseJson.put("status", "success");
					responseJson.put("message", "Exam updated successfully");
					Response.responseBuilder(200, response, responseJson);
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "No exam found");
					Response.responseBuilder(404, response, responseJson);
				}
				examDao.closeConnection();
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			int examId = Integer.parseInt(request.getParameter("id"));
			ExamDao examDao = new ExamDao();
			QuestionDao questionDao = new QuestionDao();
			if(examDao.getExam(examId) != null) {
				if(examDao.deleteExam(examId)) {
					if(questionDao.clearQuestions(examId)) {
						responseJson.put("status", "success");
						responseJson.put("message", "Exam deleted successfully");
						Response.responseBuilder(200, response, responseJson);
					}else {
						responseJson.put("status", "error");
						responseJson.put("message", "There is an error");
						Response.responseBuilder(400, response, responseJson);
					}
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "There is an error");
					Response.responseBuilder(400, response, responseJson);
				}
			}else {
				responseJson.put("status", "error");
				responseJson.put("message", "No exam found");
				Response.responseBuilder(404, response, responseJson);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "success");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
	}

}
