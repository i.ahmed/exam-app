package com.exam.controler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.exam.dao.AnswerDao;
import com.exam.dao.ExamDao;
import com.exam.dao.QuestionDao;
import com.exam.models.Answer;
import com.exam.models.Question;
import com.exam.utils.Response;
import com.google.gson.Gson;

/**
 * Servlet implementation class Questions
 */
@WebServlet(name = "questions", urlPatterns = { "/questions" })
public class Questions extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			int examID = Integer.parseInt(request.getParameter("eid"));
			int questionId = request.getParameter("qid") != null ? Integer.parseInt(request.getParameter("qid")) : 0;
			QuestionDao questionDao = new QuestionDao();
			ExamDao examDao = new ExamDao();
			if(examDao.getExam(examID) != null) {
				if(questionId != 0) {
					Question question = questionDao.getQuestion(questionId);
					if(question != null) {
						Response.responseBuilder(200, response, question);
					}else {
						responseJson.put("status", "error");
						responseJson.put("message", "No question found");
						Response.responseBuilder(404, response, responseJson);
					}
				}else {
					ArrayList<Question> questions = questionDao.getQuestions(examID);

					if(questions != null) {
						Integer[] questionsIndexs = questionDao.generateQuestionsIndexsArray(questions.size());
						Integer[] suffledQuestionIndexs = questionDao.shuffleArray(questionsIndexs);
						
						ArrayList<Question> randomizedQuestions = new ArrayList<>();
						
						for (int i = 0; i < suffledQuestionIndexs.length; i++) {
							int index = suffledQuestionIndexs[i];
							randomizedQuestions.add(questions.get(index));
						}
						
						Response.responseBuilder(200, response, randomizedQuestions);
					}else {
						responseJson.put("status", "error");
						responseJson.put("message", "No questions found");
						Response.responseBuilder(404, response, responseJson);
					}
				}
			}else {
				responseJson.put("status", "error");
				responseJson.put("message", "No Exam found");
				Response.responseBuilder(404, response, responseJson);
			}
			examDao.closeConnection();
			questionDao.closeConnection();
		} catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			int examId = Integer.parseInt(request.getParameter("eid"));
			String jsonBody = new BufferedReader(new InputStreamReader(request.getInputStream())).lines().collect(
		            Collectors.joining("\n"));
			ExamDao examDao = new ExamDao();
			if(examDao.getExam(examId) != null) {
				Gson json = new Gson();
				Question questionObj = json.fromJson(jsonBody, Question.class);
				questionObj.setEid(examId);
				QuestionDao questionDao = new QuestionDao();
				if(questionDao.addQuestion(questionObj)) {
					int qid = questionDao.getInsertedQuestionID();
					ArrayList<Answer> answers = questionObj.getAnswers();
					AnswerDao answerDao = new AnswerDao();
					for(Answer answer : answers) {
						answer.setEid(examId);
						answer.setQid(qid);
						answerDao.addAnswer(answer);
					}
					responseJson.put("status", "success");
					responseJson.put("message", "Question created successfully");
					Response.responseBuilder(201, response, responseJson);
					answerDao.closeConnection();
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "There is an error");
					Response.responseBuilder(400, response, responseJson);
				}
				questionDao.closeConnection();
			}else {
				responseJson.put("status", "error");
				responseJson.put("message", "No exam found");
				Response.responseBuilder(404, response, responseJson);
			}
			examDao.closeConnection();
			
		}catch(Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			int examId = Integer.parseInt(request.getParameter("eid"));
			String jsonBody = new BufferedReader(new InputStreamReader(request.getInputStream())).lines().collect(
		            Collectors.joining("\n"));
			ExamDao examDao = new ExamDao();
			if(examDao.getExam(examId) != null) {
				Gson json = new Gson();
				Question questionObj = json.fromJson(jsonBody, Question.class);
				QuestionDao questionDao = new QuestionDao();
				if(questionDao.updateQuestion(questionObj)) {
					int qid = questionObj.getQid();
					ArrayList<Answer> answers = questionObj.getAnswers();
					AnswerDao answerDao = new AnswerDao();
					if(answerDao.clearAnswers(qid, "qid")) {
						for(Answer answer : answers) {
							answer.setEid(examId);
							answer.setQid(qid);
							answerDao.addAnswer(answer);
						}
						responseJson.put("status", "success");
						responseJson.put("message", "Question updated successfully");
						Response.responseBuilder(200, response, responseJson);
					}else {
						responseJson.put("status", "error");
						responseJson.put("message", "There is an error");
						Response.responseBuilder(400, response, responseJson);
					}
					answerDao.closeConnection();
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "There is an error");
					Response.responseBuilder(400, response, responseJson);
				}
				questionDao.closeConnection();
			}else {
				responseJson.put("status", "error");
				responseJson.put("message", "No exam found");
				Response.responseBuilder(404, response, responseJson);
			}
			examDao.closeConnection();
			
		}catch(Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			int id = Integer.parseInt(request.getParameter("id"));
			QuestionDao questionDao = new QuestionDao();
			if(questionDao.getQuestion(id) != null){
				if(questionDao.deleteQuestion(id)) {
					responseJson.put("status", "success");
					responseJson.put("message", "Question deleted successfully");
					Response.responseBuilder(200, response, responseJson);
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "There is an error");
					Response.responseBuilder(400, response, responseJson);
				}
			}else {
				responseJson.put("status", "error");
				responseJson.put("message", "No question found");
				Response.responseBuilder(404, response, responseJson);
			}
			questionDao.closeConnection();
		} catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
	}

}
