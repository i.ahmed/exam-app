package com.exam.controler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.exam.dao.AnswerDao;
import com.exam.dao.ExamDao;
import com.exam.dao.QuestionDao;
import com.exam.dao.StudentDao;
import com.exam.models.Answer;
import com.exam.models.Exam;
import com.exam.models.Question;
import com.exam.models.Student;
import com.exam.utils.Response;

@WebServlet("/reports/answers")
public class StudentAnswersReport extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			int sid = request.getParameter("sid") != null ? Integer.parseInt(request.getParameter("sid")) : 0;
			int eid = request.getParameter("eid") != null ? Integer.parseInt(request.getParameter("eid")) : 0;
			
			if(sid == 0 || eid == 0) {
				responseJson.put("status", "error");
				responseJson.put("message", "Student or exam is missing...");
				Response.responseBuilder(400, response, responseJson);
				return;
			}
			
			StudentDao studentDao = new StudentDao();
			Student student = studentDao.getStudent(sid);
			ExamDao examDao = new ExamDao();
			Exam exam = examDao.getExam(eid);
			
			if(student == null || exam == null) {
				responseJson.put("status", "error");
				responseJson.put("message", "Student or exam not found...");
				Response.responseBuilder(404, response, responseJson);
				return;
			}
			
			QuestionDao questionDao = new QuestionDao();
			AnswerDao answerDao = new AnswerDao();
			
			ArrayList<Question> questions = questionDao.getQuestions(eid);
			
			if(questions != null) {
				for (Question question : questions) {
					ArrayList<Answer> studentAnswers = answerDao.getAnswersByStudentIdAndExamIdAndQuestionId(sid, eid, question.getQid());
					
					if(studentAnswers != null) {
						for (Answer answer : studentAnswers) {
							question
							.getAnswers()
							.stream()
							.forEach(item -> {
								if(item.getAid() == answer.getAid())
									item.setChecked(true);
							});
							
						}
					}else {
						responseJson.put("status", "error");
						responseJson.put("message", "No records found");
						Response.responseBuilder(404, response, responseJson);
						return;
					}
				}
				
				Response.responseBuilder(200, response, questions);
			} else {
				responseJson.put("status", "error");
				responseJson.put("message", "No records found");
				Response.responseBuilder(404, response, responseJson);
			}
			
			examDao.closeConnection();
			questionDao.closeConnection();
			
		} catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
