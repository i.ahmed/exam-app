package com.exam.controler;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.exam.dao.StudentDao;
import com.exam.models.Student;
import com.exam.utils.Response;

/**
 * Servlet implementation class StudentAuth
 */
@WebServlet(name = "auth", urlPatterns = { "/auth" })
public class StudentAuth extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			String username = request.getParameter("username").trim();
			String password = request.getParameter("password").trim();
			int specId = Integer.parseInt(request.getParameter("spec_id"));
			if(username.length() > 0 && password.length() > 0 && specId != 0) {
				Student student = new Student();
				student.setUsername(username);
				student.setPassword(password);
				student.setSpecId(specId);
				StudentDao studentDao = new StudentDao();
				student = studentDao.loginStudent(student);
				if(student != null) {
					Response.responseBuilder(200, response, student);
					return;
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "Unable to login");
					Response.responseBuilder(404,response, responseJson);	
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", "There is an error");
			Response.responseBuilder(400,response, responseJson);	
		}
	}

}
