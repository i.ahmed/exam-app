package com.exam.controler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.exam.dao.ExamDao;
import com.exam.dao.StudentDao;
import com.exam.models.ExamSpec;
import com.exam.models.Student;
import com.exam.utils.Response;

@WebServlet(name = "students", urlPatterns = { "/students" })
public class Students extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			int sid = request.getParameter("id") != null ? Integer.parseInt(request.getParameter("id")) : 0;
			StudentDao studentDao = new StudentDao();
			if(sid != 0) {
				Student student = studentDao.getStudent(sid);
				if(student != null) {
					Response.responseBuilder(200, response, student);
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "No student found");
					Response.responseBuilder(404, response, responseJson);
				}
			}else {
				ArrayList<Student> students = studentDao.getStudents();
				if(students != null) {
					Response.responseBuilder(200, response, students);
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "No students found");
					Response.responseBuilder(404, response, responseJson);
				}
			}
			studentDao.closeConnection();
		} catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			String username = request.getParameter("username").trim();
			String password = request.getParameter("password").trim();
			int specId = Integer.parseInt(request.getParameter("spec_id"));
			String specName = request.getParameter("spec_name");
			float schoolAvg = 0.0f;
			if(request.getParameter("school_avg") != null || !request.getParameter("school_avg").trim().equals(""))
				schoolAvg = Float.parseFloat(request.getParameter("school_avg"));
			if(username.length() > 0 && password.length() > 0 && schoolAvg != 0.0f) {
				StudentDao studentDao = new StudentDao();
				Student student = studentDao.getStudent(username, password);
				
				if(student == null) {
					System.out.println("no student found");
					
					// upload from file
					if(specId == 0) {
						ExamDao examDao = new ExamDao();
						ExamSpec spec = examDao.getSpec(specName); 
						
						if(spec != null) {
							specId = spec.getSpec_id();
						}else {
							specId = 18;
						}
						
					}
					
					System.out.println("-------------------");
					System.out.println("spec_id : " + specId);
					System.out.println("spec_name : " + specName);
					System.out.println("school_avg : " + schoolAvg);
					System.out.println("username : " + username);
					System.out.println("password : " + password);
					
					student = new Student(username, password, specId, schoolAvg);
					if(studentDao.AddStudent(student)) {
						responseJson.put("status", "success");
						responseJson.put("message", "Student created successfully");
						Response.responseBuilder(201, response, responseJson);
					} else {
						responseJson.put("status", "error");
						responseJson.put("message", "This username already in use");
						Response.responseBuilder(400, response, responseJson);
					}
				}else {
					System.out.println("student found");
					responseJson.put("status", "error");
					responseJson.put("message", "Student already exists");
					Response.responseBuilder(400, response, responseJson);
				}
				studentDao.closeConnection();
			}else {
				System.out.println("student found");
				responseJson.put("status", "error");
				responseJson.put("message", "All fields required");
				Response.responseBuilder(400, response, responseJson);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
	}

	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			String username = request.getParameter("username").trim();
			String password = request.getParameter("password") != null ? request.getParameter("password").trim() : null;
			float schoolAvg = 0.0f;
			if(request.getParameter("school_avg") != null && !request.getParameter("school_avg").trim().equals(""))
				schoolAvg = Float.parseFloat(request.getParameter("school_avg"));
			int sid = Integer.parseInt(request.getParameter("id"));
			int specId = request.getParameter("spec_id")!= null ? Integer.parseInt(request.getParameter("spec_id")) : 0;
			if(username.length() > 0 && schoolAvg != 0.0f) {
				Student student = new Student();
				student.setSid(sid);
				if(specId == 0) 
					specId = student.getSpecId();
				student.setSpecId(specId);
				student.setUsername(username);
				if(password != null)
					student.setPassword(password);
				if(schoolAvg != 0.0f) 
					student.setSchoolAvg(schoolAvg);
				StudentDao  studentDao = new StudentDao();
				int result = studentDao.updateStudent(student);
				studentDao.closeConnection();
				if(result == 1) {
					responseJson.put("status", "error");
					responseJson.put("message", "No student found");
					Response.responseBuilder(404, response, responseJson);
				}else if(result == 2) {
					responseJson.put("status", "success");
					responseJson.put("message", "Student updated successfully");
					Response.responseBuilder(200, response, responseJson);
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "There is an error");
					Response.responseBuilder(400, response, responseJson);
				}
			}else {
				responseJson.put("status", "error");
				responseJson.put("message", "All fields are required");
				Response.responseBuilder(400, response, responseJson);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
	}

	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try{
			int id = Integer.parseInt(request.getParameter("id"));
			StudentDao studentDao = new StudentDao();
			int result = studentDao.deleteStudent(id);
			studentDao.closeConnection();
			if(result == 1) {
				responseJson.put("status", "error");
				responseJson.put("message", "No student found");
				Response.responseBuilder(404, response, responseJson);
			}else if(result == 2) {
				responseJson.put("status", "success");
				responseJson.put("message", "Student deleted successfully");
				Response.responseBuilder(200, response, responseJson);
			}else {
				responseJson.put("status", "error");
				responseJson.put("message", "There is an error");
				Response.responseBuilder(400, response, responseJson);
			}
		}catch(Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
			
		}
		
	}

}
