package com.exam.controler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.exam.dao.ExamDao;
import com.exam.dao.QuestionDao;
import com.exam.dao.StudentDao;
import com.exam.dao.UserDao;
import com.exam.utils.Response;
import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

/**
 * Servlet implementation class Summary
 */
@WebServlet(name = "summarys", urlPatterns = { "/summary" })
public class Summarys extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, Object> responseJson = new HashMap<String, Object>();
		try {
			QuestionDao qDao = new QuestionDao();
			ExamDao eDao = new ExamDao();
			UserDao uDao = new UserDao();
			StudentDao sDao = new StudentDao();
			//statistics
			HashMap<String, Object> statistics = new HashMap<String, Object>();
			statistics.put("student_count", sDao.count());
			statistics.put("user_count",  uDao.count());
			statistics.put("exam_count",  eDao.count());
			statistics.put("question_count",  qDao.count());
			responseJson.put("statistics", statistics);
			//chart data
			ArrayList<HashMap<String, Object>> examStudentCountSummary = eDao.getExamStudentCountSummary();
			ArrayList<HashMap<String, Object>> examStudentPassCountSummary = eDao.getExamStudentPassCountSummary();
			
			if(examStudentCountSummary != null)
				responseJson.put("examStudentCountSummary",examStudentCountSummary);
			else
				responseJson.put("examStudentCountSummary", new ArrayList());
			
			if(examStudentPassCountSummary != null) 
				responseJson.put("examStudentPassCountSummary",examStudentPassCountSummary);
			else
				responseJson.put("examStudentPassCountSummary", new ArrayList());
			
			Response.responseBuilder(200, response, responseJson);
			
			qDao.closeConnection();
			eDao.closeConnection();
			sDao.closeConnection();
			uDao.closeConnection();
		}catch(Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", e.getMessage());
			Response.responseBuilder(400, response, responseJson);
		}
		
	}

}
