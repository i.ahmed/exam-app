package com.exam.controler;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.exam.dao.UserDao;
import com.exam.models.User;
import com.exam.utils.Response;

@WebServlet(name = "users", urlPatterns = { "/users" })
public class Users extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			String username = request.getParameter("username").trim();
			String password = request.getParameter("password").trim();
			if(username.length() > 0 && password.length() > 0) {
				User user = new User();
				user.setUsername(username);
				user.setPassword(password);
				UserDao userDao = new UserDao();
				user = userDao.loginUser(user);
				if(user != null) {
					Response.responseBuilder(400, response, user);
					return;
				}else {
					responseJson.put("status", "error");
					responseJson.put("message", "Unable to login");
					Response.responseBuilder(404,response, responseJson);	
				}
				userDao.closeConnection();
			}
		}catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", "There is an error");
			Response.responseBuilder(400,response, responseJson);	
		}
	   
	}

	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, String> responseJson = new HashMap<String, String>();
		try {
			String username = request.getParameter("username").trim();
			String password = request.getParameter("password").trim();
			int id = Integer.parseInt(request.getParameter("id"));
			User user = new User();
			if(username.length() > 0) {
				user.setUsername(username);
			}
			if(password.length() > 0) {
				user.setPassword(password);
			}
			UserDao userDao = new UserDao();
			user.setId(id);
			if(userDao.UpdateUser(user)) {
				responseJson.put("status", "success");
				responseJson.put("message", "Profile Updated Successfully");
				Response.responseBuilder(200, response, responseJson);
				return;
			}else {
				responseJson.put("status", "error");
				responseJson.put("message", "There is an error");
				Response.responseBuilder(400,response, responseJson);	
			}
		}catch (Exception e) {
			e.printStackTrace();
			responseJson.put("status", "error");
			responseJson.put("message", "There is an error");
			Response.responseBuilder(400,response, responseJson);	
		}
	}
	

}
