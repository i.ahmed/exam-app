package com.exam.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.exam.models.Answer;

public class AnswerDao {
	private Connection con;
	public AnswerDao() throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		con = DriverManager.getConnection("jdbc:mysql://localhost/exam", "root", "");
	}
	public void closeConnection() throws SQLException {
		this.con.close();
	}
	public ArrayList<Answer> getAnswers(int questionId) {
		try {
			PreparedStatement stmt = con.prepareStatement("select * from answers where qid=?");
			stmt.setInt(1, questionId);
			ResultSet result = stmt.executeQuery();
			ArrayList<Answer> answers = new ArrayList<Answer>();
			while(result.next()) {
				Answer answer = new Answer();
				answer.setAid(result.getInt("aid"));
				answer.setEid(result.getInt("eid"));
				answer.setContent(result.getString("content"));
				answer.setCorrect(result.getBoolean("isCorrect"));
				answer.setQid(result.getInt("qid"));
				answers.add(answer);
			}
			if(answers.size() > 0) 
				return answers;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<Answer> getCorrectAnswers(int questionId) {
		try {
			PreparedStatement stmt = con.prepareStatement("select * from answers where qid=? and isCorrect = 1");
			stmt.setInt(1, questionId);
			ResultSet result = stmt.executeQuery();
			ArrayList<Answer> answers = new ArrayList<Answer>();
			while(result.next()) {
				Answer answer = new Answer();
				answer.setAid(result.getInt("aid"));
				answer.setEid(result.getInt("eid"));
				answer.setContent(result.getString("content"));
				answer.setCorrect(result.getBoolean("isCorrect"));
				answer.setQid(result.getInt("qid"));
				answers.add(answer);
			}
			if(answers.size() > 0) 
				return answers;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<Answer> getAnswersByStudentIdAndExamIdAndQuestionId(int sid, int eid, int qid) {
		try {
			PreparedStatement stmt = con.prepareStatement("select * from exam_question_answer where student_id=? and exam_id=? and question_id=?");
			stmt.setInt(1, sid);
			stmt.setInt(2, eid);
			stmt.setInt(3, qid);
			
			ResultSet result = stmt.executeQuery();
			ArrayList<Answer> answers = new ArrayList<Answer>();
			
			while(result.next()) {
				Answer answer = new Answer();
				answer.setAid(result.getInt("answer_id"));
				answer.setEid(result.getInt("exam_id"));
				answer.setQid(result.getInt("question_id"));
				answers.add(answer);
			}
			
			if(answers.size() > 0) 
				return answers;
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean addAnswer(Answer answer) {
		try {
			PreparedStatement stmt = con.prepareStatement("insert into answers (eid,qid,content,isCorrect) values (?,?,?,?)");
			stmt.setInt(1, answer.getEid());
			stmt.setInt(2, answer.getQid());
			stmt.setString(3, answer.getContent());
			stmt.setBoolean(4, answer.isCorrect());
			if(stmt.executeUpdate() > 0) 
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean clearAnswers(int id, String by) {
		try {
			String sql = "";
			if(by == "eid") {
				sql = "delete from answers where eid =?";
			}else {
				sql = "delete from answers where qid=?";
			}
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			if(stmt.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
