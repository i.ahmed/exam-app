package com.exam.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.exam.models.Answer;
import com.exam.models.Exam;
import com.exam.models.ExamResult;
import com.exam.models.ExamSpec;
import com.exam.models.Question;
import com.exam.models.Student;

public class ExamDao {
	private Connection con;
	public ExamDao() throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		con = DriverManager.getConnection("jdbc:mysql://localhost/exam", "root", "");
	}
	
	public void closeConnection() throws SQLException {
		con.close();
	}
	
	public ArrayList<ExamSpec> getExamSpecs(boolean onlyActive) {
		try {
			String sql = "select * from exam_specs";
			
			if(onlyActive) {
				sql = "select * from exam_specs where is_active = 1";
			}
			
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet result = stmt.executeQuery();
			ArrayList<ExamSpec> specs = new ArrayList<ExamSpec>();
			
			while(result.next()) {
				ExamSpec spec = new ExamSpec();
				spec.setSpec_id(result.getInt("spec_id"));
				spec.setSpec_name(result.getString("spec_name"));
				spec.setActive(result.getBoolean("is_active"));
				spec.setCreatedAt(result.getDate("created_at"));
				spec.setUpdatedAt(result.getDate("updated_at"));
				specs.add(spec);
			}
			
			if(specs.size() > 0) {
				return specs;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ExamSpec getSpec(int id) {
		try {
			PreparedStatement stmt = con.prepareStatement("select * from exam_specs where spec_id=?");
			stmt.setInt(1, id);
			ResultSet result = stmt.executeQuery();
			if(result.next()) {
				ExamSpec spec = new ExamSpec();
				spec.setSpec_id(result.getInt("spec_id"));
				spec.setSpec_name(result.getString("spec_name"));
				spec.setActive(result.getBoolean("is_active"));
				return spec;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ExamSpec getSpec(String name) {
		try {
			PreparedStatement stmt = con.prepareStatement("select * from exam_specs where spec_name=?");
			stmt.setString(1, name);
			ResultSet result = stmt.executeQuery();
			if(result.next()) {
				ExamSpec spec = new ExamSpec();
				spec.setSpec_id(result.getInt("spec_id"));
				spec.setSpec_name(result.getString("spec_name"));
				spec.setActive(result.getBoolean("is_active"));
				return spec;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean addSpec(ExamSpec spec) {
		try{
			PreparedStatement stmt = con.prepareStatement("insert into exam_specs (spec_name,is_active) values(?,?)");
			stmt.setString(1, spec.getSpec_name());
			stmt.setBoolean(2, spec.isActive());
			if(stmt.executeUpdate() > 0) 
				return true;
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		
		return false;
	}
	
	public boolean updateSpec(ExamSpec spec) {
		try{
			PreparedStatement stmt = con.prepareStatement("update exam_specs set spec_name=?, is_active=? where spec_id=?");
			stmt.setString(1, spec.getSpec_name());
			stmt.setBoolean(2, spec.isActive());
			stmt.setInt(3, spec.getSpec_id());
			if(stmt.executeUpdate() > 0) 
				return true;
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		
		return false;
	}
	
	public ArrayList<Exam> getExams() {
		try {
			PreparedStatement stmt = con.prepareStatement("select e.*,s.spec_name as spec_name from exams e, exam_specs s where e.spec_id = s.spec_id");
			ResultSet result = stmt.executeQuery();
			ArrayList<Exam> exams = new ArrayList<Exam>();
			while(result.next()) {
				Exam exam = new Exam();
				exam.setSpec_id(result.getInt("spec_id"));
				exam.setEid(result.getInt("eid"));
				exam.setSubject(result.getString("subject"));
				exam.setSpec_name(result.getString("spec_name"));
				exams.add(exam);
			}
			if(exams.size() > 0) {
				return exams;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Exam getExam(int id) {
		try {
			PreparedStatement stmt = con.prepareStatement("select * from exams where eid=?");
			stmt.setInt(1, id);
			ResultSet result = stmt.executeQuery();
			if(result.next()) {
				Exam exam = new Exam();
				exam.setEid(result.getInt("eid"));
				exam.setSubject(result.getString("subject"));
				return exam;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<Exam> getStudentNewExams(Student student) {
		try {
			PreparedStatement stmt = con.prepareStatement("select e.*, s.spec_id from exams e, exam_specs s "
					+ "where eid not in (select eid from exam_results where sid=?) "
					+ "and e.spec_id = s.spec_id "
					+ "and s.is_active = 1 "
					+ "and e.spec_id=?");
			stmt.setInt(1, student.getSid());
			stmt.setInt(2, student.getSpecId());
			ResultSet result = stmt.executeQuery();
			ArrayList<Exam> exams = new ArrayList<Exam>();
			while(result.next()) {
				Exam exam = new Exam();
				exam.setEid(result.getInt("eid"));
				exam.setSubject(result.getString("subject"));
				exams.add(exam);
			}
			if(exams.size() > 0)
				return exams;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean addExam(Exam exam) {
		try{
			PreparedStatement stmt = con.prepareStatement("insert into exams (subject,spec_id) values(?,?)");
			stmt.setString(1, exam.getSubject());
			stmt.setInt(2, exam.getSpec_id());
			if(stmt.executeUpdate() > 0) 
				return true;
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		
		return false;
	}
	
	public boolean updateExam(Exam exam) {
		try{
			PreparedStatement stmt = con.prepareStatement("update exams set subject=?,spec_id=? where eid=?");
			stmt.setString(1, exam.getSubject());
			stmt.setInt(2, exam.getSpec_id());
			stmt.setInt(3, exam.getEid());
			if(stmt.executeUpdate() > 0) 
				return true;
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		
		return false;
	}
	
	public boolean deleteExam(int id) {
		try {
			PreparedStatement stmt = con.prepareStatement("delete from exams where eid=?");
			stmt.setInt(1, id);
			if(stmt.executeUpdate() > 0) 
				return true;
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		
		return false;
	}
	
	public boolean submitExamResult(int eid, int sid, double mark) {
		try {
			PreparedStatement stmt = con.prepareStatement("insert into exam_results(eid, sid, mark) values (?,?,?)");
			stmt.setInt(1, eid);
			stmt.setInt(2, sid);
			stmt.setDouble(3, mark);
			if(stmt.executeUpdate() > 0) 
				return true;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean saveQuestionResult(int eid, int qid, int sid,int aid) {
		try {
			PreparedStatement stmt = con.prepareStatement("insert into exam_question_answer(exam_id, student_id,question_id,answer_id) values (?,?,?,?)");
			stmt.setInt(1, eid);
			stmt.setInt(2, sid);
			stmt.setInt(3, qid);
			stmt.setInt(4, aid);
			if(stmt.executeUpdate() > 0) 
				return true;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public ArrayList<ExamResult> getExamResults(int sid, int eid, boolean isReport) {
		try {
			String sql = "";
			
			if(!isReport) {
				sql = "SELECT r.*, e.subject FROM exam_results r, exams e WHERE r.sid = ? and r.eid = e.eid";
			}else if(sid != 0 && eid != 0) {
//				sql =   "SELECT r.*, e.subject,s.username,s.school_avg"+
//						" FROM exam_results r, exams e, students s"+
//						" WHERE r.eid = e.eid " +
//						" and r.sid = ? and r.eid = ? and s.sid=r.sid" +
//						" and r.mark = (SELECT max(mark) from exam_results where eid = e.eid and sid = s.sid)" +
//						" GROUP by s.sid,e.eid,r.mark";
				sql =   "SELECT r.eid as eid,s.sid, e.subject,s.username,s.school_avg,"
						+ "CEIL((100 / (SELECT COUNT(*) from questions q where q.eid = e.eid)) * r.mark) as mark "
						+ "FROM exam_results r, exams e, students s "
						+ "WHERE r.eid = e.eid "
						+ "and r.sid = ? and r.eid = ? "
						+ "and s.sid=r.sid "
						+ "and e.eid <> 1 "
						+ "and r.mark = (SELECT max(rs.mark) from exam_results rs where rs.eid = e.eid and sid = s.sid) "
						+ "GROUP by s.sid,e.eid,r.mark;";
			}else if(sid!= 0) {
//				sql =   "SELECT r.*, e.subject,s.username,s.school_avg"+
//						" FROM exam_results r, exams e, students s"+
//						" WHERE r.eid = e.eid " +
//						" and r.sid = ? and s.sid=r.sid" +
//						" and r.mark = (SELECT max(mark) from exam_results where eid = e.eid and sid = s.sid)" +
//						" GROUP by s.sid,e.eid,r.mark";
				sql =   "SELECT r.eid as eid,s.sid, e.subject,s.username,s.school_avg,"
						+ "CEIL((100 / (SELECT COUNT(*) from questions q where q.eid = e.eid)) * r.mark) as mark "
						+ "FROM exam_results r, exams e, students s "
						+ " WHERE r.eid = e.eid "
						+ " and r.sid = ?"
						+ " and s.sid=r.sid "
						+ " and e.eid <> 1 "
						+ " and r.mark = (SELECT max(rs.mark) from exam_results rs where rs.eid = e.eid and sid = s.sid) "
						+ " GROUP by s.sid,e.eid,r.mark;";
			}else if(eid!=0) {
//				sql =   "SELECT r.*, e.subject,s.username,s.school_avg"+
//						" FROM exam_results r, exams e, students s"+
//						" WHERE r.eid = e.eid " +
//						" and r.eid = ? and s.sid=r.sid" +
//						" and r.mark = (SELECT max(mark) from exam_results where eid = e.eid and sid = s.sid)" +
//						" GROUP by s.sid,e.eid,r.mark  ";
				
				sql =   "SELECT r.eid as eid,s.sid, e.subject,s.username,s.school_avg,"
						+ "CEIL((100 / (SELECT COUNT(*) from questions q where q.eid = e.eid)) * r.mark) as mark "
						+ "FROM exam_results r, exams e, students s "
						+ " WHERE r.eid = e.eid "
						+ " and r.eid = ?"
						+ " and s.sid=r.sid "
						+ " and e.eid <> 1 "
						+ " and r.mark = (SELECT max(rs.mark) from exam_results rs where rs.eid = e.eid and sid = s.sid) "
						+ " GROUP by s.sid,e.eid,r.mark;";
			}else {
//				sql =   "SELECT r.*, e.subject,s.username,s.school_avg"+
//						" FROM exam_results r, exams e, students s"+
//						" WHERE r.eid = e.eid " +
//						" and s.sid=r.sid" +
//						" and r.mark = (SELECT max(mark) from exam_results where eid = e.eid and sid = s.sid)" +
//						" GROUP by s.sid,e.eid,r.mark  ";
				sql =   "SELECT r.eid as eid,s.sid, e.subject,s.username,s.school_avg,"
						+ "CEIL((100 / (SELECT COUNT(*) from questions q where q.eid = e.eid)) * r.mark) as mark "
						+ "FROM exam_results r, exams e, students s "
						+ " WHERE r.eid = e.eid "
						+ " and s.sid=r.sid "
						+ " and e.eid <> 1 "
						+ " and r.mark = (SELECT max(rs.mark) from exam_results rs where rs.eid = e.eid and sid = s.sid) "
						+ " GROUP by s.sid,e.eid,r.mark;";
			}
			
			PreparedStatement stmt = con.prepareStatement(sql);
			
			
			if(!isReport) {
				stmt.setInt(1, sid);
			}else if(sid!= 0 && eid != 0) {
				stmt.setInt(1, sid);
				stmt.setInt(2, eid);
			}else if(sid != 0){
				stmt.setInt(1, sid);
			}else if(eid!=0) {
				stmt.setInt(1, eid);
			}
			ResultSet result = stmt.executeQuery();
			ArrayList<ExamResult> results = new ArrayList<ExamResult>();
			while(result.next()) {
				ExamResult examResult = new ExamResult();
				examResult.setStudent("");
				examResult.setSid(result.getInt("sid"));
				if(isReport)
					examResult.setStudent(result.getString("username"));
				examResult.setExamId(result.getInt("eid"));
				examResult.setExamName(result.getString("subject"));
				examResult.setScore(result.getDouble("mark"));
				examResult.setPoint(getExamPoints(result.getDouble("mark")));
				examResult.setSchoolAvg(result.getFloat("school_avg"));
				double finalMark = (examResult.getSchoolAvg() * 0.3) + (examResult.getScore() * 0.7);
				examResult.setAppreciation(getResultAppreciation(finalMark));
				examResult.setFinalMark(Double.parseDouble(new DecimalFormat("##.#").format(finalMark)));
				results.add(examResult);
			}
			
			if(results.size() > 0) 
				return results;
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String getResultAppreciation(double score) {
		if(score >= 90) 
			return "Excelent";
		else if(score >= 85 && score < 90) 
			return "Very Good (+)";
		else if(score >= 80 && score < 85) 
			return "Very Good";
		else if(score >= 75 && score < 80) 
			return "Good (+)";
		else if(score >= 65 && score < 75) 
			return "Good";
		else if(score >= 60 && score < 65) 
			return "Acceptable (+)";
		else if(score >= 50 && score < 60) 
			return "Acceptable";
		else if(score >= 30 && score < 50) 
			return "Week";
		
		return "Very Week";
	}
	
	public double getExamPoints(double score) {
		if(score >= 90) 
			return 4;
		else if(score >= 85 && score < 90) 
			return 3.5;
		else if(score >= 80 && score < 85) 
			return 3.0;
		else if(score >= 75 && score < 80) 
			return 2.5;
		else if(score >= 65 && score < 75) 
			return 2.0;
		else if(score >= 60 && score < 65) 
			return 1.5;
		else if(score >= 50 && score < 60) 
			return 1.0;
		else if(score >= 30 && score < 50) 
			return 0.5;
		
		return 0.1;
	}
	
	public int count() {
		try {
			PreparedStatement stmt = con.prepareStatement("select count(eid) as count from exams");
			ResultSet result = stmt.executeQuery();
			if(result.next()) 
				return result.getInt("count");
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		return 0;
	}
	
	
	public ArrayList<HashMap<String, Object>> getExamStudentCountSummary() {
		try {
			PreparedStatement stmt = con.prepareStatement("select count(r.sid) as count , e.subject from exam_results r, exams e where r.eid=e.eid group by r.eid");
			ResultSet result = stmt.executeQuery();
			ArrayList<HashMap<String, Object>> records = new ArrayList<HashMap<String, Object>>();
			while(result.next()) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put(result.getString("subject"), result.getInt("count"));
				records.add(map);
			}
			if(records.size() > 0) 
				return records;
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		return null;
	}
	
	public ArrayList<HashMap<String, Object>> getExamStudentPassCountSummary() {
		try {
			PreparedStatement stmt = con.prepareStatement(
					"SELECT e.subject ,count(r.rid) as count from exam_results r, exams e "
					+ "where mark >= 50 and e.eid = r.eid GROUP by r.eid");
			ResultSet result = stmt.executeQuery();
			ArrayList<HashMap<String, Object>> records = new ArrayList<HashMap<String, Object>>();
			while(result.next()) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put(result.getString("subject"), result.getInt("count"));
				records.add(map);
			}
			if(records.size() > 0) 
				return records;
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		return null;
	}
	
	public double calculateStudentAnswers(int sid, int eid,AnswerDao answerDao, QuestionDao qd) throws Exception {
		
		
		ArrayList<Question> questions = qd.getQuestions(eid);
		
		double examScore = 0.0f;
		
		for(Question question : questions) {
			ArrayList<Answer> studentAnswers = answerDao.getAnswersByStudentIdAndExamIdAndQuestionId(sid, eid, question.getQid());
			
			if(studentAnswers != null) {
				Map<Integer, Integer> answersSet = new HashMap<>();
				System.out.println("sid : " + sid  + " qid : " + question.getQid());
				studentAnswers.forEach((answer) -> {
					answersSet.put(answer.getQid(), answer.getAid());
				});
				
				int[] answerIds = answersSet.values().stream().mapToInt(Integer::intValue).toArray();
				
				double score = qd.matchCorrectAnswer(question.getQid(), answerIds);
				examScore += score;
			}
		}
				
		
		return examScore;
		
		
	}

}
	
