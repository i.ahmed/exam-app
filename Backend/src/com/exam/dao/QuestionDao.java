package com.exam.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.exam.models.Answer;
import com.exam.models.Question;

public class QuestionDao {
	private Connection con;
	public QuestionDao() throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		con = DriverManager.getConnection("jdbc:mysql://localhost/exam", "root", "");
	}
	
	public void closeConnection() throws SQLException {
		this.con.close();
	}
	
	public Question getQuestion(int id) {
		try {
			PreparedStatement stmt = 
					con.prepareStatement("select q.*,count(s.aid) as correct_answers from questions q, answers s where q.qid = ? and s.qid = q.qid and s.isCorrect = 1");
			stmt.setInt(1, id);
			ResultSet result = stmt.executeQuery();
			if(result.next()) {
				Question question = new Question();
				AnswerDao answerDao = new AnswerDao();
				ArrayList<Answer> answers = answerDao.getAnswers(id);
				question.setEid(result.getInt("eid"));
				question.setQid(result.getInt("qid"));
				question.setCorrectAnswers(result.getInt("correct_answers"));
				question.setQusetion(result.getString("question"));
				question.setAnswers(answers);
				answerDao.closeConnection();
				return question;
			}			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public ArrayList<Question> getQuestions(int examID) {
		try {
			PreparedStatement stmt = 
					con.prepareStatement("select q.*,count(s.aid) as correct_answers from questions q, answers s where q.eid=? and s.qid = q.qid and s.isCorrect = 1 group by q.qid");
			stmt.setInt(1, examID);
			ResultSet result = stmt.executeQuery();
			ArrayList<Question> questions = new ArrayList<Question>();
			while(result.next()) {
				AnswerDao answerDao = new AnswerDao();
				ArrayList<Answer> answers = answerDao.getAnswers(result.getInt("qid"));
				Question question = new Question();
				question.setEid(examID);
				question.setQid(result.getInt("qid"));
				question.setCorrectAnswers(result.getInt("correct_answers"));
				question.setQuestion(result.getString("question"));
				question.setAnswers(answers);
				questions.add(question);
				answerDao.closeConnection();
			}
			if(questions.size() > 0)
				return questions;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Integer[] generateQuestionsIndexsArray(int size) {
		Integer[] arr = new Integer[size];
        
        List<Integer> range = 
        		IntStream
        		.rangeClosed(0, arr.length - 1)
        		.boxed()
        		.collect(Collectors.toList());
                            
        range.toArray(arr);
        
       return arr;
	}
	
	public Integer[] shuffleArray(Integer[] arr) {

		List<Integer> intList = Arrays.asList(arr);

		Collections.shuffle(intList);

		intList.toArray(arr);

		return arr;
     }
	
	
	public int getInsertedQuestionID() {
		try {
			PreparedStatement stmt = con.prepareStatement("select qid from questions order by qid desc limit 1");
			ResultSet result = stmt.executeQuery();
			if(result.next())
				return result.getInt("qid");
		}catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public boolean addQuestion(Question question) {
		try {
			PreparedStatement stmt = con.prepareStatement("insert into questions (eid,question) values (?,?)");
			stmt.setInt(1, question.getEid());
			stmt.setString(2, question.getQuestion());
			if(stmt.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean updateQuestion(Question question) {
		try {
			PreparedStatement stmt = con.prepareStatement("update questions set question=? where qid=?");
			stmt.setString(1, question.getQuestion());
			stmt.setInt(2, question.getQid());
			if(stmt.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean deleteQuestion(int id) {
		try {
			PreparedStatement stmt = con.prepareStatement("delete from questions  where qid=?");
			stmt.setInt(1, id);
			if(stmt.executeUpdate() > 0) {
				AnswerDao answerDao = new AnswerDao();
				if(answerDao.clearAnswers(id, "qid")) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean clearQuestions(int examId) {
		try {
			PreparedStatement stmt = con.prepareStatement("delete from questions  where eid=?");
			stmt.setInt(1, examId);
			if(stmt.executeUpdate() > 0) {
				AnswerDao answerDao = new AnswerDao();
				if(answerDao.clearAnswers(examId, "eid")) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public double matchCorrectAnswer(int qid, int[] answerId) {
		try {
			AnswerDao answerDao = new AnswerDao();
			ArrayList<Answer> answers = answerDao.getCorrectAnswers(qid);
			Question question = getQuestion(qid);
			
			int correctAnswersCount = 0;
			for(int id : answerId ) {
				boolean correctAnswer = answers.stream().anyMatch(answer -> answer.getAid() == id);
				if(correctAnswer) {
					correctAnswersCount++;
				}
			}
			
			
			
			int examQuestionsCount = examQuestionsCount(question.getEid());
			
			// System.out.println("correct: " + correctAnswersCount + " count: " + (100.0 / examQuestionsCount));
			
			if(correctAnswersCount == answers.size()) return (Double.valueOf(100.0 / examQuestionsCount));
			
			return Math.ceil(((100.0 / examQuestionsCount) / answers.size()) * correctAnswersCount);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public int count() {
		try {
			PreparedStatement stmt = con.prepareStatement("select count(qid) as count from questions");
			ResultSet result = stmt.executeQuery();
			if(result.next()) 
				return result.getInt("count");
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		return 0;
	}
	
	public int examQuestionsCount(int examId) {
		try {
			PreparedStatement stmt = con.prepareStatement("select count(qid) as count from questions where eid=?");
			stmt.setInt(1, examId);
			ResultSet result = stmt.executeQuery();
			if(result.next()) 
				return result.getInt("count");
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		return 0;
	}
	
}
