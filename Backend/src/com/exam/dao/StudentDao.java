/**
 * 
 */
package com.exam.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.exam.models.Student;

/**
 * @author ibrahim
 *
 */
public class StudentDao {
	private Connection con;
	
	public StudentDao() throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		con = DriverManager.getConnection("jdbc:mysql://localhost/exam", "root", "");
	}
	
	public void closeConnection() throws SQLException {
		con.close();
	}
	
	public Student loginStudent(Student student) throws SQLException {
		try {
			PreparedStatement stmt = con.prepareStatement("select * from students where username=? and password=? and spec_id=?");
			stmt.setString(1, student.getUsername());
			stmt.setString(2, student.getPassword());
			stmt.setInt(3, student.getSpecId());
			ResultSet result = stmt.executeQuery();
			if(result.next()) {
				student.setSid(result.getInt("sid"));
				student.setSpecId(result.getInt("spec_id"));
				student.setPassword(result.getString("password"));
				return student;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			if(con != null) 
				con.close();
		}
		return null;
	}
	
	public ArrayList<Student> getStudents() throws Exception {
		try{
			PreparedStatement stmt = con.prepareStatement("select s.*,sp.spec_name "
					+ "from students s, exam_specs sp "
					+ "where s.spec_id = sp.spec_id");
			ResultSet result  = stmt.executeQuery();
			ArrayList<Student> students = new ArrayList<Student>();
			while(result.next()) {
				Student student = new Student();
				student.setSid(result.getInt("sid"));
				student.setSpecName(result.getString("spec_name"));
				student.setSpecId(result.getInt("spec_id"));
				student.setUsername(result.getString("username"));
				student.setSchoolAvg(result.getFloat("school_avg"));
				students.add(student);
			}
			if(students.size() > 0) 
				return students;
			
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		return null;
	}
	public Student getStudent(int sid) throws Exception {
		try{
			PreparedStatement stmt = con.prepareStatement("select * from students where sid=?");
			stmt.setInt(1, sid);
			ResultSet result  = stmt.executeQuery();
			if(result.next()) {
				Student student = new Student();
				student.setSid(sid);
				student.setSpecId(result.getInt("spec_id"));
				student.setUsername(result.getString("username"));
				student.setSchoolAvg(result.getFloat("school_avg"));
				return student;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		return null;
	}
	
	public Student getStudent(String username, String password) throws Exception {
		try{
			PreparedStatement stmt = con.prepareStatement("select * from students where username=? and password=?");
			stmt.setString(1, username);
			stmt.setString(2, password);
			ResultSet result  = stmt.executeQuery();
			
			if(result.next()) {
				Student student = new Student();
				student.setSid(result.getInt("sid"));
				student.setSpecId(result.getInt("spec_id"));
				student.setUsername(result.getString("username"));
				return student;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		return null;
	}
	public boolean AddStudent(Student student) throws Exception {
		try {
			PreparedStatement stmt = con.prepareStatement("insert into students (username,password, spec_id, school_avg) values(?,?,?,?)");
			stmt.setString(1, student.getUsername());
			stmt.setString(2, student.getPassword());
			stmt.setInt(3, student.getSpecId());
			stmt.setFloat(4, student.getSchoolAvg());
			if(stmt.executeUpdate() > 0) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	public int updateStudent(Student student) throws Exception {
		try {
			String sql = "";
			Student currentStudent = getStudent(student.getSid());
			if(currentStudent == null) {
				return 1;
			}
			
			if(currentStudent.getUsername().equals(student.getUsername())) {
				sql = "update students set password='"+student.getPassword()+"'"+
					  ", spec_id="+ student.getSpecId() + ", school_avg="+ student.getSchoolAvg() +
						" where sid="+ student.getSid();
			}else if(student.getPassword() != null) {
				sql = "update students set username='"+student.getUsername()+
						"', password='"+student.getPassword()+
						"', spec_id="+ student.getSpecId() + ", school_avg="+ student.getSchoolAvg() +
						" where sid="+student.getSid();
			}else {
				sql = "update students set username='"+student.getUsername()+"',"+
						"spec_id="+ student.getSpecId() + ", school_avg="+ student.getSchoolAvg() +
						" where sid="+student.getSid();
			}
			
			Statement stmt = con.createStatement();
			if(stmt.executeUpdate(sql) > 0) {
				return 2;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return 3;
	}
	public int deleteStudent(int id) throws Exception {
		try {
			Student currentStudent = getStudent(id);
			if(currentStudent == null) 
				return 1;
			PreparedStatement stmt = con.prepareStatement("delete from students where sid=?");
			stmt.setInt(1, id);
			if(stmt.executeUpdate() > 0) {
				return 2;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return 3;
	}
	
	public int count() {
		try {
			PreparedStatement stmt = con.prepareStatement("select count(sid) as count from students");
			ResultSet result = stmt.executeQuery();
			if(result.next()) 
				return result.getInt("count");
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		return 0;
	}
}
