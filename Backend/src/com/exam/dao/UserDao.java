package com.exam.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.exam.models.User;

public class UserDao {
	Connection con;
	public UserDao() throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		con = DriverManager.getConnection("jdbc:mysql://localhost/exam", "root", "");
	}
	
	public void closeConnection() throws Exception {
		con.close();
	}
	
	public User loginUser(User user) throws SQLException {
		try {
			PreparedStatement stmt = con.prepareStatement("select * from users where username=? and password=?");
			stmt.setString(1, user.getUsername());
			stmt.setString(2, user.getPassword());
			ResultSet result = stmt.executeQuery();
			
			if(result.next()) {
				user.setId(result.getInt("id"));
				return user;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			if(con != null) 
				con.close();
		}
		return null;
	}
	
	public boolean UpdateUser(User user) throws SQLException {
		try {
			String sql = "";
			if(user.getPassword() == null ) {
				sql = "update users set username=? where id=?";
			}else {
				sql = "update users set username=? ,password=? where id=?";
			}
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, user.getUsername());
			if(user.getPassword() == null) {
				stmt.setInt(2, user.getId());
			}else {
				stmt.setString(2, user.getPassword());
				stmt.setInt(3, user.getId());
			}
			if(stmt.executeUpdate() > 0) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			if(con != null) 
				con.close();
		}
		return false;
	}
	
	public int count() {
		try {
			PreparedStatement stmt = con.prepareStatement("select count(id) as count from users");
			ResultSet result = stmt.executeQuery();
			if(result.next()) 
				return result.getInt("count");
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		return 0;
	}
}
