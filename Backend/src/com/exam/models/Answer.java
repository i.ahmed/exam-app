package com.exam.models;

public class Answer {
	private int aid,eid,qid;
	private String content;
	private boolean isCorrect;
	private boolean checked = false;
	public Answer() {
		super();
	}
	public Answer(int eid, int qid,int aid, String content, boolean isCorrect) {
		super();
		this.eid = eid;
		this.qid = qid;
		this.aid = aid;
		this.content = content;
		this.isCorrect = isCorrect;
	}
	public int getEid() {
		return eid;
	}
	public void setEid(int eid) {
		this.eid = eid;
	}
	public int getQid() {
		return qid;
	}
	public void setQid(int qid) {
		this.qid = qid;
	}
	public int getAid() {
		return aid;
	}
	public void setAid(int aid) {
		this.aid = aid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public boolean isCorrect() {
		return isCorrect;
	}
	public void setCorrect(boolean isCorrect) {
		this.isCorrect = isCorrect;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	
}
