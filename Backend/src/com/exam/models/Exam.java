package com.exam.models;

public class Exam {
	private int eid,spec_id;
	private String subject,spec_name;
	
	public Exam() {}

	public Exam(int eid, String subject, int spec_id, String spec_name) {
		this.eid = eid;
		this.subject = subject;
		this.spec_id = spec_id;
		this.spec_name = spec_name;
	}

	public int getEid() {
		return eid;
	}

	public void setEid(int eid) {
		this.eid = eid;
	}
	

	public int getSpec_id() {
		return spec_id;
	}

	public void setSpec_id(int spec_id) {
		this.spec_id = spec_id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSpec_name() {
		return spec_name;
	}

	public void setSpec_name(String spec_name) {
		this.spec_name = spec_name;
	}
	
	
	
	
}
