package com.exam.models;

public class ExamAnswer {
	private int questionId;
	private int[] answer;

	public ExamAnswer(int questionId, int[] answer) {
		super();
		this.questionId = questionId;
		this.answer = answer;
	}

	public ExamAnswer() {
		super();
	}

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public int[] getAnswer() {
		return answer;
	}

	public void setAnswer(int[] answer) {
		this.answer = answer;
	}
	
	

}
