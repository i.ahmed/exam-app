package com.exam.models;

public class ExamResult {
	int examId, sid;
	String examName, appreciation, student;
	double score, point, finalMark;
	float schoolAvg;
	public ExamResult(int examId,int sid, String examName,String student, String appreciation, double score, double point) {
		this.sid = sid;
		this.examId = examId;
		this.examName = examName;
		this.appreciation = appreciation;
		this.score = score;
		this.point = point;
		this.student = student;
	}
	public ExamResult() {}
	
	public int getSid() {
		return sid;
	}
	public void setSid(int sid) {
		this.sid = sid;
	}
	public int getExamId() {
		return examId;
	}
	public void setExamId(int examId) {
		this.examId = examId;
	}
	public String getExamName() {
		return examName;
	}
	public void setExamName(String examName) {
		this.examName = examName;
	}
	public String getAppreciation() {
		return appreciation;
	}
	public String getStudent() {
		return student;
	}
	public void setStudent(String student) {
		this.student = student;
	}
	public void setAppreciation(String appreciation) {
		this.appreciation = appreciation;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public double getPoint() {
		return point;
	}
	public void setPoint(double point) {
		this.point = point;
	}
	public float getSchoolAvg() {
		return schoolAvg;
	}
	public void setSchoolAvg(float schoolAvg) {
		this.schoolAvg = schoolAvg;
	}
	public double getFinalMark() {
		return finalMark;
	}
	public void setFinalMark(double finalMark) {
		this.finalMark = finalMark;
	}
	
	
	
	
}
