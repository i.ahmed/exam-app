package com.exam.models;

import java.sql.Date;

public class ExamSpec {
	private int spec_id;
	private String spec_name;
	private Date createdAt;
	private Date updatedAt;
	private boolean isActive;
	
	
	public ExamSpec() {
		super();
	}


	public ExamSpec(int spec_id, String spec_name, Date created_at, Date updated_at, Boolean isActive) {
		super();
		this.spec_id = spec_id;
		this.spec_name = spec_name;
		this.createdAt = created_at;
		this.updatedAt = updated_at;
		this.isActive = isActive;
	}


	public int getSpec_id() {
		return spec_id;
	}


	public void setSpec_id(int spec_id) {
		this.spec_id = spec_id;
	}


	public String getSpec_name() {
		return spec_name;
	}


	public void setSpec_name(String spec_name) {
		this.spec_name = spec_name;
	}


	public Date getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}


	public Date getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}


	public boolean isActive() {
		return isActive;
	}


	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	
	
}
