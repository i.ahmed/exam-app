package com.exam.models;

import java.util.ArrayList;

public class Question {
	private int qid, eid, correctAnswers;
	private  String qusetion;
	private  ArrayList<Answer> answers;
	public Question(String question, ArrayList<Answer> answers) {
		super();
		this.qusetion = question;
		this.answers = answers;
	}
	public Question() {
		super();
	}
	public int getEid() {
		return eid;
	}
	public void setEid(int eid) {
		this.eid = eid;
	}
	public int getQid() {
		return qid;
	}
	public void setQid(int qid) {
		this.qid = qid;
	}
	public int getCorrectAnswers() {
		return correctAnswers;
	}
	public void setCorrectAnswers(int correctAnswers) {
		this.correctAnswers = correctAnswers;
	}
	public String getQusetion() {
		return qusetion;
	}
	public void setQusetion(String qusetion) {
		this.qusetion = qusetion;
	}
	public String getQuestion() {
		return qusetion;
	}
	public void setQuestion(String question) {
		this.qusetion = question;
	}
	public ArrayList<Answer> getAnswers() {
		return answers;
	}
	public void setAnswers(ArrayList<Answer> answers) {
		this.answers = answers;
	}
	
	
}
