package com.exam.models;

public class Student {
	private int sid, specId;
	private String username, password, specName;
	private float schoolAvg;
	
	public Student() {}
	
	public Student(String username, String password, int specId, float schoolAvg) {
		super();
		this.username = username;
		this.password = password;
		this.specId = specId;
		this.schoolAvg = schoolAvg;
	}

	public Student(int sid, String username, String password) {
		this.sid = sid;
		this.username = username;
		this.password = password;
	}
	public int getSid() {
		return sid;
	}
	public void setSid(int sid) {
		this.sid = sid;
	}
	public int getSpecId() {
		return specId;
	}
	public void setSpecId(int specId) {
		this.specId = specId;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getSpecName() {
		return specName;
	}

	public void setSpecName(String specName) {
		this.specName = specName;
	}

	public float getSchoolAvg() {
		return schoolAvg;
	}

	public void setSchoolAvg(float schoolAvg) {
		this.schoolAvg = schoolAvg;
	}
	
	
	
	
	
}
