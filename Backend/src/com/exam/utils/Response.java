package com.exam.utils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

public class Response {
	private static Gson json = new Gson();
	
	public static void responseBuilder(int status ,HttpServletResponse response, Object data ) throws IOException {
		try {
			PrintWriter out = response.getWriter();
			response.setStatus(status);
			response.setContentType("application/json");
			response.setCharacterEncoding("utf8");
			String resJson = json.toJson(data);
			out.print(resJson);
			out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
