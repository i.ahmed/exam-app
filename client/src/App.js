import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "./pages/Login";
import Dashboard from "./pages/Dashboard";
import Profile from "./pages/Profile";
import StudentsTable from "./components/StudentsTable";
import StudentForm from "./components/StudentForm";
import ExamsTable from "./components/ExamsTable";
import ExamForm from "./components/ExamForm";
import QuestionsTable from "./components/QuestionsTable";
import QuestionForm from "./components/QuestionForm";
import Signin from "./pages/Signin";
import Main from "./pages/Main";
import ExamsList from "./components/ExamsList";
import Exam from "./pages/Exam";
import ExamResult from "./pages/ExamResult";
import StudentProfile from "./pages/StudentProfile";
import Home from "./pages/Home";
import ExamReport from "./pages/ExamReport";
import SpecsTable from "./components/SpecsTable";
import ExamSpecForm from "./components/ExamSpecForm";
import StudentAnswersReport from "./components/StudentAnswersReport";
function App() {
  return (
    <Router>
      <Switch>
        <Route path="/login" component={Login} />
        <Route
          exact
          path="/dashboard"
          children={
            <Dashboard>
              <Home />
            </Dashboard>
          }
        />
        <Route
          exact
          path="/dashboard/specs"
          children={
            <Dashboard>
              <SpecsTable />
            </Dashboard>
          }
        />
        <Route
          exact
          path="/dashboard/specs/add"
          children={
            <Dashboard>
              <ExamSpecForm />
            </Dashboard>
          }
        />
        <Route
          exact
          path="/dashboard/specs/edit/:id"
          children={
            <Dashboard>
              <ExamSpecForm />
            </Dashboard>
          }
        />
        <Route
          path="/dashboard/profile"
          children={
            <Dashboard>
              <Profile />
            </Dashboard>
          }
        />
        <Route
          exact
          path="/dashboard/students"
          children={
            <Dashboard>
              <StudentsTable />
            </Dashboard>
          }
        />
        <Route
          path="/dashboard/students/add"
          children={
            <Dashboard>
              <StudentForm />
            </Dashboard>
          }
        />
        <Route
          path="/dashboard/students/edit/:id"
          children={
            <Dashboard>
              <StudentForm />
            </Dashboard>
          }
        />
        <Route
          exact
          path="/dashboard/exams"
          children={
            <Dashboard>
              <ExamsTable />
            </Dashboard>
          }
        />
        <Route
          path="/dashboard/exams/add"
          children={
            <Dashboard>
              <ExamForm />
            </Dashboard>
          }
        />
        <Route
          exact
          path="/dashboard/exams/edit/:id"
          children={
            <Dashboard>
              <ExamForm />
            </Dashboard>
          }
        />
        <Route
          path="/dashboard/exams/:eid/setup"
          children={
            <Dashboard>
              <QuestionsTable />
            </Dashboard>
          }
        />
        <Route
          path="/dashboard/exams/:eid/question/add"
          children={
            <Dashboard>
              <QuestionForm />
            </Dashboard>
          }
        />
        <Route
          path="/dashboard/exams/:eid/question/:qid/edit"
          children={
            <Dashboard>
              <QuestionForm />
            </Dashboard>
          }
        />
        <Route
          path="/dashboard/reports/exam"
          children={
            <Dashboard>
              <ExamReport />
            </Dashboard>
          }
        />
        <Route
          path="/dashboard/reports/answers"
          children={
            <Dashboard>
              <StudentAnswersReport />
            </Dashboard>
          }
        />
        <Route exact path="/" children={<Signin />} />
        <Route
          exact
          path="/main"
          children={
            <Main>
              <ExamsList />
            </Main>
          }
        />
        <Route
          path="/exam/:eid"
          children={
            <Main>
              <Exam />
            </Main>
          }
        />
        {/* <Route
          path="/main/results"
          children={
            <Main>
              <ExamResult />
            </Main>
          }
        /> */}
        {/* <Route
          path="/main/profile"
          children={
            <Main>
              <StudentProfile />
            </Main>
          }
        /> */}
      </Switch>
    </Router>
  );
}

export default App;
