import React, { useState } from "react";
import { IoClose } from "react-icons/io5";
import { useGlobalContext } from "../context/context";

const Alert = () => {
  const { notify, setNotify } = useGlobalContext();
  const [isOpen, setIsOpen] = useState(true);
  return (
    <div className={`alert alert-${notify.status} ${isOpen ? "open" : ""}`}>
      <p>{notify.message}</p>
      <span
        onClick={() => {
          setIsOpen(!isOpen);
          setNotify({ ...notify, status: "", message: "" });
        }}
      >
        <IoClose />
      </span>
    </div>
  );
};

export default Alert;
