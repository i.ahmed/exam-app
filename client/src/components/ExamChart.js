import React from "react";
import { Bar } from "react-chartjs-2";

const ExamChart = ({ examSummary: { labels, data, colors } }) => {
  const keys = {
    labels,
    datasets: [
      {
        label: "Students",
        data,
        backgroundColor: colors,
        borderColor: colors,
        borderWidth: 1,
      },
    ],
  };

  const options = {
    indexAxis: "x",
    elements: {
      bar: {
        borderWidth: 2,
      },
    },
    maintainAspectRatio: false,
    responsive: true,
    plugins: {},
  };
  return (
    <main className="chart">
      <Bar data={keys} options={options} />
    </main>
  );
};

export default ExamChart;
