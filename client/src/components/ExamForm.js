import React, { useEffect, useRef } from "react";
import { MdSubject } from "react-icons/md";
import { useParams } from "react-router";
import { useGlobalContext } from "../context/context";
import Alert from "./Alert";

const ExamForm = () => {
  const {
    notify,
    setNotify,
    exam,
    specs,
    setSpecs,
    getSpecs,
    setExam,
    getExam,
    addExam,
    updateExam,
  } = useGlobalContext();
  const { id } = useParams();

  const handleSubmit = (e) => {
    e.preventDefault();

    if (exam.subject !== "" && exam.spec_id !== 0) {
      if (id !== undefined) {
        return updateExam(exam, notify, setNotify);
      }

      addExam(exam, setExam, notify, setNotify);
    }
  };

  const handleChange = (e) => {
    setExam({ ...exam, spec_id: parseInt(e.target.value) });
  };

  useEffect(() => {
    setExam({ ...exam, eid: 0, spec_id: 0, subject: "" });

    if (id !== undefined) {
      getExam(id, exam, setExam, notify, setNotify);
    }

    getSpecs(setSpecs, true, notify, setNotify);
  }, []);
  return (
    <main>
      {notify.status !== "" && <Alert />}
      <h2>{`${exam.eid === 0 ? "New" : "Edit"}`} Exam</h2>
      <form className="auth" onSubmit={handleSubmit}>
        <div className="form-control">
          <MdSubject />
          <input
            type="text"
            name="name"
            value={exam.subject}
            placeholder="Enter exam subject"
            autoComplete="off"
            onChange={(e) => setExam({ ...exam, subject: e.target.value })}
            required
          />
        </div>
        <div className="form-control">
          <select style={{ width: "100%" }} onChange={handleChange}>
            {exam.eid === 0 && (
              <option value="0">Select Specialization </option>
            )}
            {specs && specs.length > 0 &&
              specs.map((sepc) => {
                const { spec_id, spec_name } = sepc;
                return (
                  <option
                    value={spec_id}
                    key={spec_id}
                    defaultChecked={
                      exam.spec_id !== 0 &&
                      exam.spec_id === spec_id &&
                      "selected"
                    }
                  >
                    {spec_name}
                  </option>
                );
              })}
          </select>
        </div>
        <button type="submit">{`${exam.eid === 0 ? "add" : "update"}`}</button>
      </form>
    </main>
  );
};

export default ExamForm;
