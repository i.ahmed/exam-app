import React from "react";
import { Pie } from "react-chartjs-2";

const ExamPassChart = ({ examPassSummary: { labels, data, colors } }) => {
  const keys = {
    labels,
    datasets: [
      {
        label: "Students",
        data,
        backgroundColor: colors,
        borderColor: colors,
        borderWidth: 1,
      },
    ],
  };

  const options = {
    indexAxis: "x",
    elements: {
      bar: {
        borderWidth: 2,
      },
    },
    maintainAspectRatio: false,
    responsive: true,
    plugins: {},
  };
  return (
    <main className="chart">
      <Pie data={keys} options={options} />
    </main>
  );
};

export default ExamPassChart;
