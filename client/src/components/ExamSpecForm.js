import React, { useEffect } from "react";
import { MdSubject } from "react-icons/md";
import { useParams } from "react-router";
import { useGlobalContext } from "../context/context";
import Alert from "./Alert";

const ExamSpecForm = () => {
  const { notify, setNotify, spec, setSpec, getSpec, addSpec, updateSpec } =
    useGlobalContext();
  const { id } = useParams();

  const handleSubmit = (e) => {
    e.preventDefault();
    if (spec.spec_name !== "") {
      if (id !== undefined) {
        return updateSpec(spec, notify, setNotify);
      }
      addSpec(spec, setSpec, notify, setNotify);
    }
  };

  useEffect(() => {
    setSpec({ ...spec, spec_id: 0, spec_name: "", is_active: true });

    if (id !== undefined) {
      getSpec(id, spec, setSpec, notify, setNotify);
    }
  }, []);

  return (
    <main>
      {notify.status !== "" && <Alert />}
      <h2>{`${spec.spec_id === 0 ? "New" : "Edit"}`} Specialization</h2>
      <form className="auth" onSubmit={handleSubmit}>
        <div className="row">
          <div className="form-control" style={{ flexGrow: 1 }}>
            <MdSubject />
            <input
              type="text"
              name="name"
              value={spec.spec_name}
              placeholder="Enter Specialization Name"
              autoComplete="off"
              onChange={(e) =>
                setSpec({ ...spec, spec_name: e.target.value })
              }
              required
            />
          </div>
          <div className="check-wrapper" style={{ marginLeft: "20px" }}>
            <input
              type="checkbox"
              id="isActive"
              name="is_active"
              checked={spec.is_active && "checked"}
              onChange={(e) => {
                setSpec({ ...spec, is_active: e.target.checked });
              }}
            />
            <label htmlFor="isActive">Active</label>
          </div>
        </div>
        <button type="submit">{`${
          spec.spec_id === 0 ? "add" : "update"
        }`}</button>
      </form>
    </main>
  );
};

export default ExamSpecForm;
