import React, { useEffect } from "react";
import { MdHourglassEmpty } from "react-icons/md";
import { RiFileList2Fill } from "react-icons/ri";
import { Link } from "react-router-dom";
import { useGlobalContext } from "../context/context";

const ExamsList = () => {
  const { getUserNewExams, exams, setExams, student } = useGlobalContext();

  useEffect(() => {
    getUserNewExams(student.sid, setExams);
  }, []);

  return (
    <section className="exam-list-wrapper" style={{ maxWidth: '84%', margin: 'auto' }}>
      <header>
        <h4>Exam List</h4>
      </header>
      <ul>
        {exams && exams.length > 0 ? (
          exams.map((examItem) => {
            const { eid, subject } = examItem;
            return (
              <li key={eid}>
                <div>
                  <span>
                    <RiFileList2Fill />
                  </span>
                  <p>{subject}</p>
                </div>
                <Link to={`/exam/${eid}`}>start</Link>
              </li>
            );
          })
        ) : (
          <div className="empty-section">
            <MdHourglassEmpty fontSize="1.5rem" />
            <h4>No Exams For Now</h4>
          </div>
        )}
      </ul>
    </section>
  );
};

export default ExamsList;
