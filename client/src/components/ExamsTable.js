import React, { useEffect } from "react";
import { BiEdit, BiTrashAlt } from "react-icons/bi";
import { AiOutlinePlus } from "react-icons/ai";
import { IoSettingsOutline } from "react-icons/io5";
import { Link } from "react-router-dom";
import Alert from "./Alert";
import { useGlobalContext } from "../context/context";
const ExamsTable = () => {
  const { exams, setExams, getExams, deleteExam, notify, setNotify } =
    useGlobalContext();

  useEffect(() => {
    getExams(setExams, notify, setNotify);
  }, []);

  return (
    <main>
      {notify.status !== "" && <Alert />}
      <div className="row">
        <h2>Exams</h2>
        <Link to="/dashboard/exams/add">
          <AiOutlinePlus />
        </Link>
      </div>
      <div className="table-wrapper">
        <table>
          <thead>
            <tr>
              <th>#</th>
              <th>Specialization</th>
              <th>Subject</th>
              <th>Controls</th>
            </tr>
          </thead>
          <tbody>
            {exams && exams.length > 0 &&
              exams.map((exam) => {
                const { eid, subject, spec_name } = exam;
                return (
                  <tr key={eid}>
                    <td>{eid}</td>
                    <td>{spec_name}</td>
                    <td>{subject}</td>
                    <td>
                      <Link to={`/dashboard/exams/edit/${eid}`}>
                        <BiEdit fontSize="1.5rem" />
                      </Link>
                      {/* <span
                        onClick={() => {
                          if (
                            window.confirm(
                              "Are you sure about removeing this item"
                            )
                          ) {
                            deleteExam(eid, notify, setNotify);
                          }
                        }}
                      >
                        <BiTrashAlt fontSize="1.5rem" />
                      </span> */}
                      <Link to={`/dashboard/exams/${eid}/setup`}>
                        <IoSettingsOutline fontSize="1.5rem" />
                      </Link>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>
    </main>
  );
};

export default ExamsTable;
