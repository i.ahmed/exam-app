import React from "react";
import { useHistory } from "react-router";
import StudentInfo from "./StudentInfo";

const Header = ({ title, closeMenu, setCloseMenu, children }) => {
  const baseUrl =
    useHistory().location.pathname.indexOf("dashboard") !== -1
      ? "/dashboard"
      : "/main";

  return (
    <header className="header">
      <div className="header__container">
        <a href={`${baseUrl}`} className="header__logo">
          {`${title ? title : "Dashboard"}`}
        </a>
        {!children ? (
          <>
          
          <div className="header__toggle">
            <i
              className={`bx bx-menu ${closeMenu ? "bx-x" : ""}`}
              id="header-toggle"
              onClick={() => {
                setCloseMenu(!closeMenu);
              }}
            ></i>
          </div>
          </>
        ) : (
          <>
           {children}
          </>
        )}
      </div>
    </header>
  );
};

export default Header;
