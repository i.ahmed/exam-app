import React from "react";
import { ImCheckmark } from "react-icons/im";
import { RiCloseFill } from "react-icons/ri";
import { useGlobalContext } from "../context/context";

export default function MultiChoice({
  isCorrect = false,
  content,
  qusetion,
  setQusetion,
  answerId,
  disabled = false,
  examAnswers = null,
  setExamAnswers = null,
  currentIndex = 0,
  answerMarkType = null,
}) {
  const { addExamAnswers, findQuestionIndex } = useGlobalContext();

  function handelChange(value) {
    const answers = qusetion.answers;

    answers.map((answer, index) => {
      if (index === value || answer.aid === answerId) {
        answer.isCorrect = !answer.isCorrect;
      }

      return answer;
    });

    setQusetion({ ...qusetion, answers });
  }

  const questionIndex = examAnswers
    ? findQuestionIndex(examAnswers, qusetion.qid)
    : -1;

  const answerExists =
    examAnswers ?
    examAnswers.length > 0 &&
    questionIndex > -1 &&
    examAnswers[questionIndex]["answer"].find((answer) => answer === answerId) !== undefined
    : isCorrect;
    

  return (
    <article style={{ display: "flex", alignItems: "center" }}>
      <input
        type="checkbox"
        id={answerId}
        style={{ marginRight: 5 }}
        checked={
          answerExists
          // !examAnswers // in dashboard mode
          //   ? isCorrect
          //     ? true
          //     : false
          //   : examAnswers.length > 0 // exam mode
          //   ? answerExists
          //     ? true // answer id added to the question answer
          //     : false // answer id not added yet to the question answer
          //   : false // in begining of the exam all answers is not checked by defualt
        }
        disabled={disabled}
        onChange={(e) => {
          if (!disabled) {
            handelChange(answerId);
            if (setExamAnswers) {
              addExamAnswers(
                qusetion["qid"],
                answerId,
                examAnswers,
                setExamAnswers,
                true,
                e.target.checked
              );
            }
          }
        }}
      />
      <label htmlFor={answerId} style={{ marginRight: 5 }}>
        {content}
      </label>
      {answerMarkType !== null ? (
        answerMarkType === "success" ? (
          <span className="success">
            <ImCheckmark />
          </span>
        ) : (
          <span className="danger">
            <RiCloseFill />
          </span>
        )
      ) : (
        ""
      )}
    </article>
  );
}
