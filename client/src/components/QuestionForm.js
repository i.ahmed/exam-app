import React, { useEffect } from "react";
import { AiOutlinePlus } from "react-icons/ai";
import { BsQuestionSquare } from "react-icons/bs";
// import { IoClose } from "react-icons/io5";
import { RiQuestionAnswerLine } from "react-icons/ri";
import { useParams } from "react-router";
import { useGlobalContext } from "../context/context";
import Alert from "./Alert";
import MultiChoice from "./MultiChoice";
import SingleChoice from "./SingleChoice";

const QuestionForm = () => {
  const {
    notify,
    setNotify,
    qusetion,
    setQusetion,
    addQuestion,
    updateQuestion,
    answer,
    setAnswer,
    addAnswer,
    // deleteAnswer,
    getQuestion,
  } = useGlobalContext();

  const { eid } = useParams();
  const { qid } = useParams();

  const handleSubmit = (e) => {
    e.preventDefault();
    if (qusetion.qusetion === "") {
      return setNotify({
        ...notify,
        status: "error",
        message: "Question field is required",
      });
    }
    const wrongAnswers = qusetion.answers.filter(
      (answerItem) => answerItem.isCorrect === false
    );
    if (
      qusetion.answers.length === 0 ||
      qusetion.answers.length === 1 ||
      wrongAnswers.length === 0
    ) {
      return setNotify({
        ...notify,
        status: "error",
        message: "Add at least 2 answers one correct and one not correct",
      });
    }
    const correctAnswers = qusetion.answers.filter(
      (answerItem) => answerItem.isCorrect === true
    );
    if (correctAnswers.length === 0) {
      return setNotify({
        ...notify,
        status: "error",
        message: "Adding the correct answer is required",
      });
    }

    if (qid !== undefined) {
      return updateQuestion(eid, qusetion, notify, setNotify);
    }
    addQuestion(eid, qusetion, setQusetion, notify, setNotify);
  };
  const addNewAnswer = () => {
    addAnswer(notify, setNotify, answer, setAnswer, qusetion, setQusetion);
  };

  useEffect(() => {
    if (qid !== undefined) {
      getQuestion(eid, qid, notify, setNotify, qusetion, setQusetion);
    }

    setQusetion({
      ...qusetion, 
      eid: 0,
      qid: 0,
      correctAnswers: 0,
      qusetion: "",
      answers: []
    })
    
  }, [qid]);

  return (
    <main>
      {notify.status !== "" && <Alert />}
      <h2>{`${qusetion.qid === 0 ? "New" : "Edit"}`} Question</h2>
      <form className="auth" onSubmit={handleSubmit}>
        <div className="form-control">
          <BsQuestionSquare />
          <input
            type="text"
            name="question"
            value={qusetion.qusetion}
            placeholder="Enter question"
            autoComplete="off"
            onChange={(e) =>
              setQusetion({ ...qusetion, qusetion: e.target.value })
            }
            required
          />
        </div>
        <div className="row-input">
          <div className="form-control">
            <RiQuestionAnswerLine />
            <input
              type="text"
              name="answer"
              value={answer.content}
              placeholder="Enter answer"
              autoComplete="off"
              onChange={(e) =>
                setAnswer({ ...answer, content: e.target.value })
              }
            />
          </div>
          <div className="check-wrapper">
            <input
              type="checkbox"
              id="isCorrect"
              name="isCorrect"
              onChange={(e) => {
                setAnswer({ ...answer, isCorrect: e.target.checked });
              }}
            />
            <label htmlFor="isCorrect">Is Correct</label>
          </div>
          <span onClick={addNewAnswer}>
            <AiOutlinePlus fontSize="1.3rem" />
          </span>
        </div>
        <section className="answers-wrapper">
          {qusetion.answers && qusetion.answers.length > 0 &&
            qusetion.answers.map((answerItem, index) => {
              const { content, isCorrect } = answerItem;
              return (
                <MultiChoice
                  key={index}
                  content={content}
                  isCorrect={isCorrect}
                  qusetion={qusetion}
                  setQusetion={setQusetion}
                  answerId={index}
                />
              )
              // ) : qusetion.correctAnswers > 1 ? (
              //   <MultiChoice
              //     key={index}
              //     content={content}
              //     isCorrect={isCorrect}
              //     qusetion={qusetion}
              //     setQusetion={setQusetion}
              //     answerId={index}
              //   />
              // ) : (
              //   <SingleChoice
              //     key={index}
              //     content={content}
              //     isCorrect={isCorrect}
              //     qusetion={qusetion}
              //     setQusetion={setQusetion}
              //     answerId={index}
              //   />
              // );
            })}
        </section>
        <button type="submit">
          {`${qusetion.qid === 0 ? "add" : "update"}`}
        </button>
      </form>
    </main>
  );
};

export default QuestionForm;
