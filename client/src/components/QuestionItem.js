import React, { useEffect } from "react";
import { BiEdit, BiTrashAlt } from "react-icons/bi";
// import { RiArrowLeftSLine } from "react-icons/ri";
import { Link } from "react-router-dom";
import { useGlobalContext } from "../context/context";
import MultiChoice from "./MultiChoice";
import SingleChoice from "./SingleChoice";

const QuestionItem = ({
  eid,
  qid,
  qusetion,
  answers,
  disabled = false,
  showMarkIcon,
  isReport = false,
  qusetionHolder
}) => {
  const {
    notify,
    setNotify,
    setQuestions,
    getQuestions,
    deleteQuestion,
    setQusetion,
  } = useGlobalContext();

  useEffect(() => {
    if (notify.status === "success") {
      getQuestions(eid, notify, setNotify, setQuestions);
    }
  }, [notify]);

  const correctAnswers =
    answers && answers.filter((answer) => answer.isCorrect === true);

  function markQuestion(isCorrect, checked) {
    if (isCorrect && checked) return true;
    return false;
  }

  return (
    <div className="card">
      <div className="row">
        <label htmlFor="toggle1">{qusetion}</label>
        <div className="actions">
          <Link to={`/dashboard/exams/${eid}/question/${qid}/edit`}>
            <BiEdit fontSize="1.5rem" />
          </Link>
          {/* <span onClick={() => deleteQuestion(qid, notify, setNotify)}>
            <BiTrashAlt fontSize="1.5rem" />
          </span> */}
        </div>
      </div>
      <section className="answers-wrapper">
        {answers &&
          answers.length > 0 &&
          answers.map((answerItem, index) => {
            const { content, isCorrect, checked } = answerItem;
            return correctAnswers.length > 1 ? (
              <MultiChoice
                key={index}
                content={content}
                isCorrect={isReport ? checked : isCorrect}
                qusetion={qusetionHolder}
                setQusetion={setQusetion}
                answerId={index}
                disabled={disabled}
                answerMarkType={
                  showMarkIcon
                    ? isCorrect
                      ? "success"
                      : "error"
                    : null
                }
              />
            ) : (
              <SingleChoice
                key={index}
                content={content}
                isCorrect={isReport ? checked : isCorrect}
                qusetion={qusetionHolder}
                setQusetion={setQusetion}
                answerId={index}
                disabled={disabled}
                answerMarkType={
                  showMarkIcon
                    ? isCorrect ? "success" : "error"
                    : null
                    // ? isCorrect && checked
                    //   ? "success"
                    //   : "error"
                    // : null
                }
              />
            );
          })}
      </section>
    </div>
  );
};

export default QuestionItem;
