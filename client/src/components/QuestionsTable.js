import React, { useEffect } from "react";
import { AiOutlinePlus } from "react-icons/ai";
// import { BiEdit, BiTrashAlt } from "react-icons/bi";
// import { IoSettingsOutline } from "react-icons/io5";
import { Link, useParams } from "react-router-dom";
import { useGlobalContext } from "../context/context";
import Alert from "./Alert";
import QuestionItem from "./QuestionItem";

const QuestionsTable = () => {
  const { questions, setQuestions, getQuestions, notify, setNotify } =
    useGlobalContext();
  const { eid } = useParams();

  useEffect(() => {
    setQuestions([]);
    
    if (eid !== undefined) {
      getQuestions(eid, notify, setNotify, setQuestions, false);
    }
  }, []);

  return (
    <main>
      {notify.status !== "" && <Alert />}
      <div className="row">
        <h2>Questions</h2>
        <Link to={`/dashboard/exams/${eid}/question/add`}>
          <AiOutlinePlus />
        </Link>
      </div>
      {questions &&
        questions.length > 0 &&
        questions.map((questionItem) => {
          return (
            <QuestionItem
              {...questionItem}
              key={questionItem.qid}
              disabled={true}
              isReport={false}
              qusetionHolder={questionItem}
            />
          );
        })}
    </main>
  );
};

export default QuestionsTable;
