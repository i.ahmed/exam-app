import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { useGlobalContext } from "../context/context";

const Sidebar = ({ closeMenu }) => {
  const { logoutUser, user, setUser } = useGlobalContext();
  const [page, setPage] = useState("home");
  const history = useHistory();
  const changePage = (evt, page) => setPage(page);
  return (
    <div className={`nav ${closeMenu ? "show-menu" : ""}`} id="navbar">
      <nav className="nav__container">
        <div>
          <Link to="/dashboard" className="nav__link nav__logo">
            <i className="bx bxs-disc nav__icon"></i>
            <span className="nav__logo-name">DASHBOARD</span>
          </Link>
          <div className="nav__list">
            <div className="nav__items">
              <Link
                to="/dashboard"
                className={`nav__link ${page === "home" ? "active" : ""}`}
                onClick={(e) => changePage(e, "home")}
              >
                <i className="bx bx-home nav__icon"></i>
                <span className="nav__name ">Home</span>
              </Link>
              <Link
                to="/dashboard/specs"
                className={`nav__link ${page === "specs" ? "active" : ""}`}
                onClick={(e) => changePage(e, "specs")}
              >
                <i className="bx bx-purchase-tag-alt nav__icon"></i>
                <span className="nav__name ">Specializations</span>
              </Link>
              <div className="nav__dropdown">
                <Link
                  to="/dashboard/profile"
                  className={`nav__link ${page === "profile" ? "active" : ""}`}
                  onClick={(e) => changePage(e, "profile")}
                >
                  <i className="bx bx-user nav__icon"></i>
                  <span className="nav__name">Profile</span>
                  <i className="bx bx-chevron-down nav__icon nav__dropdown-icon"></i>
                </Link>

                <div className="nav__dropdown-collapse">
                  <div className="nav__dropdown-content">
                    <Link
                      to="/dashboard/profile"
                      className="nav__dropdown-item active"
                      onClick={(e) => changePage(e, "profile")}
                    >
                      Update
                    </Link>
                  </div>
                </div>
              </div>
              <div className="nav__dropdown">
                <Link
                  to="/dashboard/students"
                  className={`nav__link ${page === "students" ? "active" : ""}`}
                  onClick={(e) => changePage(e, "students")}
                >
                  <i className="bx bx-face nav__icon"></i>
                  <span className="nav__name">Students</span>
                  <i className="bx bx-chevron-down nav__icon nav__dropdown-icon"></i>
                </Link>

                <div className="nav__dropdown-collapse">
                  <div className="nav__dropdown-content">
                    <Link
                      to="/dashboard/students"
                      className="nav__dropdown-item active"
                      onClick={(e) => changePage(e, "students")}
                    >
                      Records
                    </Link>
                  </div>
                </div>
              </div>
              <div className="nav__dropdown">
                <Link
                  to="/dashboard/exams"
                  className={`nav__link ${page === "exams" ? "active" : ""}`}
                  onClick={(e) => changePage(e, "exams")}
                >
                  <i className="bx bx-notepad nav__icon"></i>
                  <span className="nav__name">Exams</span>
                  <i className="bx bx-chevron-down nav__icon nav__dropdown-icon"></i>
                </Link>

                <div className="nav__dropdown-collapse">
                  <div className="nav__dropdown-content">
                    <Link
                      to="/dashboard/exams"
                      className="nav__dropdown-item active"
                      onClick={(e) => changePage(e, "exams")}
                    >
                      Records
                    </Link>
                  </div>
                </div>
              </div>
              <div className="nav__dropdown">
                <span className="nav__link">
                  <i className="bx bx-book-content nav__icon"></i>
                  <span className="nav__name">Reports</span>
                  <i className="bx bx-chevron-down nav__icon nav__dropdown-icon"></i>
                </span>

                <div className="nav__dropdown-collapse">
                  <div className="nav__dropdown-content">
                    <Link
                      to="/dashboard/reports/exam"
                      className="nav__dropdown-item active"
                      onClick={(e) => changePage(e, "examReport")}
                    >
                      Exam
                    </Link>
                    <Link
                      style={{ marginTop: "10px" }}
                      to="/dashboard/reports/answers"
                      className="nav__dropdown-item active"
                      onClick={(e) => changePage(e, "examReport")}
                    >
                      Answers
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <span
          className="nav__link nav__logout"
          onClick={() => logoutUser(history, user, setUser)}
        >
          <i className="bx bx-log-out nav__icon"></i>
          <span className="nav__name">Log Out</span>
        </span>
      </nav>
    </div>
  );
};

export default Sidebar;
