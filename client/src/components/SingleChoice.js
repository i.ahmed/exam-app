import React from "react";
import { ImCheckmark } from "react-icons/im";
import { RiCloseFill } from "react-icons/ri";
import { useGlobalContext } from "../context/context";

export default function SingleChoice({
  isCorrect = false,
  content,
  qusetion,
  setQusetion,
  answerId,
  disabled = false,
  examAnswers = null,
  setExamAnswers = null,
  currentIndex = 0,
  answerMarkType = null,
}) {
  const { addExamAnswers, findQuestionIndex } = useGlobalContext();

  function handelChange(aid) {
    const answers = qusetion.answers;

    answers.map((answer, index) => {
      if (index !== aid && answer.aid !== answerId) {
        answer.isCorrect = false;
      } else {
        answer.isCorrect = true;
      }

      return answer;
    });

    setQusetion({ ...qusetion, answers });
  }

  const questionIndex = examAnswers
    ? findQuestionIndex(examAnswers, qusetion.qid)
    : -1;

  // const answerCheced =
  //   examAnswers && examAnswers.length > 0 && questionIndex !== -1
  //     ? examAnswers[questionIndex]["answer"][0] === answerId
  //     : false;

  const answerExists =
    examAnswers ?
    examAnswers.length > 0 &&
    questionIndex > -1 &&
    examAnswers[questionIndex]["answer"].find((answer) => answer === answerId) !== undefined
    : isCorrect;

  return (
    <article>
      <input
        type="radio"
        id={`option-${answerId}`}
        name={`option-${qusetion.qid}`}
        checked={// !examAnswers ? isCorrect : examAnswers.length > 0 ? answerCheced : false
          answerExists
          // !examAnswers // in dashboard mode
          //   ? isCorrect
          //     // ? true
          //     // : false
          //   : examAnswers.length > 0 // exam mode
          //     ? answerExists
          //       // ? true // answer id added to the question answer
          //       // : false // answer id not added yet to the question answer
          //     : false 
        }// in begining of the exam all answers is not checked by defualt}
        disabled={disabled}
        onChange={(e) => {
          if (!disabled) {
            e.target.checked = true;
            handelChange(answerId);
            if (setExamAnswers) {
              addExamAnswers(
                qusetion["qid"],
                answerId,
                examAnswers,
                setExamAnswers
              );
            }
          }
        }}
      />
      <label htmlFor={`option-${answerId}`}> {content} </label>
      {answerMarkType !== null ? (
        answerMarkType === "success" ? (
          <span className="success">
            <ImCheckmark />
          </span>
        ) : (
          <span className="danger">
            <RiCloseFill />
          </span>
        )
      ) : (
        ""
      )}
    </article>
  );
}
