import React, { useEffect } from "react";
import { BiEdit } from "react-icons/bi";
import { AiOutlinePlus } from "react-icons/ai";
import { ImSwitch } from "react-icons/im";
import { Link } from "react-router-dom";
import Alert from "./Alert";
import { useGlobalContext } from "../context/context";
import { updateSpec } from "../utils/specs";
const SpecsTable = () => {
  const { specs, setSpecs, getSpecs, notify, setNotify } = useGlobalContext();

  useEffect(() => {
    getSpecs(setSpecs,false, notify, setNotify);
  }, []);

  return (
    <main>
      {notify.status !== "" && <Alert />}
      <div className="row">
        <h2>Specialties</h2>
        <Link to="/dashboard/specs/add">
          <AiOutlinePlus />
        </Link>
      </div>
      <div className="table-wrapper">
        <table>
          <thead>
            <tr>
              <th>#</th>
              <th>name</th>
              <th>Controls</th>
            </tr>
          </thead>
          <tbody>
            {specs && specs.length > 0 &&
              specs.map((spec) => {
                const { spec_id, spec_name, isActive } = spec;
                return (
                  <tr key={spec_id}>
                    <td>{spec_id}</td>
                    <td>{spec_name}</td>
                    <td>
                      <Link to={`/dashboard/specs/edit/${spec_id}`}>
                        <BiEdit fontSize="1.5rem" />
                      </Link>
                      <Link
                        to={`#`}
                        onClick={async (e) => {
                          e.preventDefault();
                          await updateSpec(
                            { spec_id, spec_name, is_active: !isActive },
                            notify,
                            setNotify
                          );
                          getSpecs(setSpecs,false, notify, setNotify);
                        }}
                      >
                        <ImSwitch
                          fontSize="1.5rem"
                          color={!isActive ? "red": undefined}
                        />
                      </Link>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>
    </main>
  );
};

export default SpecsTable;
