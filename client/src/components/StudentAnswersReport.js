import React, { useRef, useEffect } from "react";
import { useGlobalContext } from "../context/context";
import Alert from "./Alert";
import QuestionItem from "./QuestionItem";

const StudentAnswersReport = () => {
  const {
    students,
    setStudents,
    notify,
    setNotify,
    getStudents,
    setAnswers,
    questions,
    setQuestions,
    exams,
    setExams,
    getExams,
    fetchStudentExamAnswers,
  } = useGlobalContext();

  const studentRef = useRef();
  const examRef = useRef();

  function handleSubmit(e) {
    e.preventDefault();

    const sid = parseInt(studentRef.current.value);
    const eid = parseInt(examRef.current.value);

    setQuestions([]);

    fetchStudentExamAnswers(sid, eid, setQuestions, notify, setNotify);
  }

  useEffect(() => {
    getStudents(setStudents, notify, setNotify);
    getExams(setExams, notify, setNotify);
    setQuestions([]);
  }, []);

  return (
    <main>
      {notify.status !== "" && <Alert />}
      <form onSubmit={handleSubmit}>
        <div className="row" style={{ alignItems: "center", marginBottom: 20 }}>
          <div
            className="form-control"
            style={{ marginBottom: 0, flexGrow: "1", marginRight: 10 }}
          >
            <select ref={studentRef} style={{ width: "100%" }}>
              <option value="0">Select Student</option>
              {students && students.length > 0 &&
                students.map((student) => {
                  const { sid, username } = student;
                  return (
                    <option value={sid} key={sid}>
                      {username}
                    </option>
                  );
                })}
            </select>
          </div>
          <div
            className="form-control"
            style={{ marginBottom: 0, flexGrow: "1", marginRight: 10 }}
          >
            <select ref={examRef} style={{ width: "100%" }}>
              <option value="0">Select Exam </option>
              {exams && exams.length > 0 &&
                exams.map((exam) => {
                  const { eid, subject } = exam;
                  return (
                    <option value={eid} key={eid}>
                      {subject}
                    </option>
                  );
                })}
            </select>
          </div>
          <button type="submit" style={{ width: "fit-content" }}>
            Fetch
          </button>
        </div>
      </form>
      {questions && questions.length > 0 &&
        questions.map((questionItem) => {
          return (
            <QuestionItem
              {...questionItem}
              qusetionHolder={questionItem}
              key={questionItem.qid}
              disabled={true}
              showMarkIcon={true}
              isReport={true}
            />
          );
        })}
    </main>
  );
};

export default StudentAnswersReport;
