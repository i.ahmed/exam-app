import React, { useEffect } from "react";
import { AiOutlineUser, AiOutlineLock } from "react-icons/ai";
import { useParams } from "react-router";
import { useGlobalContext } from "../context/context";
import Alert from "./Alert";
const StudentForm = () => {
  const {
    notify,
    setNotify,
    addStudent,
    updateStudent,
    student,
    getStudent,
    setStudent,
    specs,
    getSpecs,
    setSpecs,
  } = useGlobalContext();

  const { id } = useParams();

  const handleSubmit = (e) => {
    e.preventDefault();
    if (student.username === "" || student.specId === 0) {
      return setNotify({
        ...notify,
        status: "error",
        message: "All fields are required",
      });
    }

    if (id !== undefined) {
      return updateStudent(student, notify, setNotify);
    }

    if (student.password !== "") {
      return addStudent(student,'',true, notify, setNotify);
    }

    setNotify({
      ...notify,
      status: "error",
      message: "All fields are required",
    });
  };

  useEffect(() => {
    setStudent({
      ...student,
      sid: 0,
      username: "",
      specId: 0,
      password: "",
      schoolAvg: 0.0
    });

    if (id !== undefined) {
      getStudent(id, student, setStudent, notify, setNotify);
    }

    getSpecs(setSpecs,false, notify, setNotify);
  }, []);

  return (
    <main>
      {notify.status !== "" && <Alert />}
      <h2>{`${student.sid === 0 ? "New" : "Edit"}`} Student</h2>
      <form className="auth" onSubmit={handleSubmit}>
        <div className="form-control">
          <AiOutlineUser />
          <input
            type="text"
            name="username"
            value={student.username}
            placeholder="Enter student username"
            autoComplete="off"
            onChange={(e) =>
              setStudent({ ...student, username: e.target.value })
            }
            required
          />
        </div>
        <div className="form-control">
          <AiOutlineLock />
          <input
            type="password"
            name="password"
            placeholder="Enter complex password"
            value={student.password}
            onChange={(e) =>
              setStudent({ ...student, password: e.target.value })
            }
            required
          />
        </div>
        <div className="form-control">
          <AiOutlineLock />
          <input
            type="text"
            name="average"
            placeholder="Enter High School Average"
            value={student.schoolAvg}
            onChange={(e) =>
              setStudent({ ...student, schoolAvg: e.target.value })
            }
            required
          />
        </div>
        <div className="form-control">
          <select
            style={{ width: "100%", outline: "none", border: "none" }}
            onChange={(e) =>
              setStudent({ ...student, specId: parseInt(e.target.value) })
            }
          >
            {!id && <option value="0"> Select Specialization </option>}
            {specs && specs.length > 0 &&
              specs.map((sepc) => {
                const { spec_id, spec_name } = sepc;
                return (
                  <option
                    value={spec_id}
                    key={spec_id}
                    defaultChecked={spec_id === student.specId}
                  >
                    {spec_name}
                  </option>
                );
              })}
          </select>
        </div>
        <button type="submit">
          {`${student.sid === 0 ? "add" : "update"}`}
        </button>
      </form>
    </main>
  );
};

export default StudentForm;
