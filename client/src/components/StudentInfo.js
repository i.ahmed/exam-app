import React from 'react';
import { AiOutlineUser } from 'react-icons/ai';
import { useGlobalContext } from '../context/context';
import logo from '../img/default-img.png';

export default function StudentInfo({ isOpend }) {
    const {student} = useGlobalContext();
    const imagepath = `/students-images/${student.img}.jpg`;

    return (
        <React.Fragment>
            <h4 style={{ marginRight: '5px' }}> {student.username} </h4>
            <AiOutlineUser fontSize="1.5rem" style={{ cursor: "pointer" }} />
            <section style={{
                background: '#fff',
                position: 'absolute',
                left: '50%',
                right: '50%',
                transform: ' translate(-50%, 72%)',
                width: '200px',
                height: '150px',
                border: '1px solid #eee',
                borderRadius: 5,
                padding: '8px',
                display: isOpend ? 'block' : 'none'
            }}>
                <img src={imagepath || logo} alt='Student Image' style={{ height: '133px', width: '100%' }} />
            </section>
        </React.Fragment>
    )
}