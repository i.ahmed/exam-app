import React, { useEffect, useState, useRef } from "react";
import { BiEdit, BiTrashAlt } from "react-icons/bi";
import { AiOutlineLock, AiOutlinePlus } from "react-icons/ai";
import { Link } from "react-router-dom";
import Alert from "./Alert";
import { useGlobalContext } from "../context/context";

const StudentsTable = () => {
  const {
    students,
    setStudents,
    getStudents,
    uploadStudents,
    notify,
    setNotify,
  } = useGlobalContext();
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    getStudents(setStudents, notify, setNotify);
  }, []);

  const studentsFileRef = useRef();

  // useEffect(() => {
  //   if (refresh) {
  //     getStudents(setStudents, notify, setNotify);
  //     setRefresh(false);
  //   }
  // }, [refresh]);

  return (
    <main>
      {notify.status !== "" && <Alert />}
      <div className="row">
        <h2>Students</h2>
        {/* <form>
            <section className="row" style={{ alignItems: "center" }}>
              <div className="form-control" style={{ marginBottom: "0px", padding: '8px' }}>
                  <AiOutlineLock />
                  <input
                    type="file"
                    name="students-file"
                    ref={studentsFileRef}
                    required
                  />
              </div>
              <button 
                  type="button" 
                  style={{ width: 'auto', fontSize: "14px", marginLeft: '10px' }} 
                  onClick={(e) => studentsFileRef.current.files.length === 0 ? alert("Choose File Please") : uploadStudents(studentsFileRef.current.files[0], notify, setNotify)}>UPLOAD
              </button>
            </section>
        </form> */}
        <Link to="/dashboard/students/add">
          <AiOutlinePlus />
        </Link>
      </div>
      <div className="table-wrapper">
        <table>
          <thead>
            <tr>
              <th>#</th>
              <th>Username</th>
              <th>Specialization</th>
              <th>Average</th>
              <th>Controls</th>
            </tr>
          </thead>
          <tbody>
            {students && students.length > 0 &&
              students.map((student) => {
                const { sid, username, specName, schoolAvg } = student;
                return (
                  <tr key={sid}>
                    <td>{sid}</td>
                    <td>{username}</td>
                    <td>{specName}</td>
                    <td>{schoolAvg}</td>
                    <td>
                      <Link to={`/dashboard/students/edit/${sid}`}>
                        <BiEdit fontSize="1.5rem" />
                      </Link>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>
    </main>
  );
};

export default StudentsTable;
