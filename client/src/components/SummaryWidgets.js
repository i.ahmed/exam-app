import React from "react";
import { RiAdminLine, RiFileListLine } from "react-icons/ri";
import { FiUsers } from "react-icons/fi";
import { AiOutlineQuestion } from "react-icons/ai";
const SummaryWidgets = ({
  userCount,
  studentCount,
  examCount,
  questionCount,
}) => {
  return (
    <section className="widgets">
      <article>
        <div className="content">
          <div>
            <h2>{userCount}</h2>
            <h4>Users</h4>
          </div>
          <span>
            <RiAdminLine />
          </span>
        </div>
      </article>
      <article>
        <div className="content">
          <div>
            <h2>{studentCount}</h2>
            <h4>Students</h4>
          </div>
          <span>
            <FiUsers />
          </span>
        </div>
      </article>
      <article>
        <div className="content">
          <div>
            <h2>{examCount}</h2>
            <h4>Exams</h4>
          </div>
          <span>
            <RiFileListLine />
          </span>
        </div>
      </article>
      <article>
        <div className="content">
          <div>
            <h2>{questionCount}</h2>
            <h4>Questions</h4>
          </div>
          <span>
            <AiOutlineQuestion />
          </span>
        </div>
      </article>
    </section>
  );
};

export default SummaryWidgets;
