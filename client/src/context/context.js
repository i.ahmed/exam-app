import React, { useContext, useState } from "react";
import * as User from "../utils/user";
import * as Spec from "../utils/specs";
import * as Student from "../utils/student";
import * as Exam from "../utils/exam";
import * as Question from "../utils/question";
import * as Answer from "../utils/answer";
import * as Summary from "../utils/summary";

const AppContext = React.createContext();
const AppProvider = ({ children }) => {
  const currentUser = localStorage.getItem("user")
    ? JSON.parse(localStorage.getItem("user"))
    : {};
  const [title, setTitle] = useState("");
  const [user, setUser] = useState({
    id: currentUser.id || 0,
    username: currentUser.username || "",
    password: "",
  });
  const [notify, setNotify] = useState({
    status: "",
    message: "",
  });
  const currentStudent = localStorage.getItem("student")
    ? JSON.parse(localStorage.getItem("student"))
    : {};
  const [student, setStudent] = useState({
    sid: currentStudent.sid || 0,
    username: currentStudent.username || "",
    specId: currentStudent.specId || 0,
    schoolAvg: currentStudent.schoolAvg || 0.0,
    password: '',
    img: currentStudent.img
  });
  const [spec, setSpec] = useState({
    spec_id: 0,
    spec_name: "",
    is_active: true,
  });
  const [exam, setExam] = useState({
    eid: 0,
    spec_id: 0,
    subject: "",
  });
  const [qusetion, setQusetion] = useState({
    eid: 0,
    qid: 0,
    correctAnswers: 0,
    qusetion: "",
    answers: [],
  });
  const [answer, setAnswer] = useState({
    eid: 0,
    qid: 0,
    aid: 0,
    content: "",
    isCorrect: false,
  });
  const [students, setStudents] = useState([]);
  const [exams, setExams] = useState([]);
  const [specs, setSpecs] = useState([]);
  const [questions, setQuestions] = useState(
    JSON.parse(localStorage.getItem("questions")) || []
  );
  const [answers, setAnswers] = useState([]);

  return (
    <AppContext.Provider
      value={{
        title,
        setTitle,
        user,
        setUser,
        student,
        setStudent,
        students,
        setStudents,
        specs,
        setSpecs,
        spec,
        setSpec,
        exams,
        setExams,
        exam,
        setExam,
        questions,
        setQuestions,
        qusetion,
        setQusetion,
        answer,
        setAnswer,
        notify,
        setNotify,
        answers,
        setAnswers,
        loading: false,
        ...User,
        ...Student,
        ...Spec,
        ...Exam,
        ...Question,
        ...Answer,
        ...Summary,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

const useGlobalContext = () => {
  return useContext(AppContext);
};

export { useGlobalContext, AppProvider };
