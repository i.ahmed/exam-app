import React, { useEffect, useState } from "react";
import Header from "../components/Header";
import Sidebar from "../components/Sidebar";
import { useGlobalContext } from "../context/context";
import "../dashboard.css";
const Dashboard = ({ history, children }) => {
  const { user } = useGlobalContext();
  const [closeMenu, setCloseMenu] = useState(false);
  useEffect(() => {
    if (user.id === 0) {
      history.replace("/login");
    }
  }, [user, history]);
  return (
    <React.Fragment>
      <Header closeMenu={closeMenu} setCloseMenu={setCloseMenu} />
      <Sidebar closeMenu={closeMenu} />
      {children}
    </React.Fragment>
  );
};

export default Dashboard;
