import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import { BsChevronLeft, BsChevronRight } from "react-icons/bs";
import { useGlobalContext } from "../context/context";
import SingleChoice from "../components/SingleChoice";
import MultiChoice from "../components/MultiChoice";

const Exam = () => {
  const {
    setTitle,
    getExam,
    getQuestions,
    questions,
    setQuestions,
    qusetion,
    setQusetion,
    exam,
    setExam,
    notify,
    setNotify,
    submitExamAnswers
  } = useGlobalContext();
  const { eid } = useParams();
  const history = useHistory();

  const [examAnswers, setExamAnswers] = useState(
    localStorage.getItem("examAnswers") !== null
      ? JSON.parse(localStorage.getItem("examAnswers"))
      : []
  );
  const [currentQuestion, setCurrentQuestion] = useState(
    examAnswers.length === 0 ? 0 : examAnswers.length - 1
  );

  useEffect(() => {
    setTitle(exam.subject);
    getExam(eid, exam, setExam, notify, setNotify);

    if (localStorage.getItem("questions")) {
      return;
    }

    getQuestions(eid, notify, setNotify, setQuestions);
  }, []);

  useEffect(() => {
    if (questions.length > 0) {
      setQusetion({ ...qusetion, ...questions[currentQuestion] });
    }
  }, [currentQuestion, questions]);

  return (
    <main className="exam" style={{ maxWidth: '84%', margin: 'auto' }}>
      <header className="row">
        <span
          title="Previous Question"
          className={`${currentQuestion === 0 ? "disabled-button" : ""}`}
          onClick={() =>
            setCurrentQuestion((currentIndex) => (currentIndex -= 1))
          }
        >
          <BsChevronLeft /> Previous
        </span>
        <span>
          {currentQuestion + 1}/{questions.length}
        </span>
        <span
          title="Next Question"
          className={`${currentQuestion === questions.length - 1 ? "disabled-button" : ""
            }`}
          onClick={() =>
            setCurrentQuestion((currentIndex) => (currentIndex += 1))
          }
        >
          Next <BsChevronRight />
        </span>
      </header>
      <section>
        {questions && questions.length > 0 && (
          <React.Fragment>
            <h4>{qusetion["qusetion"]}</h4>
            <section className="answers-wrapper" style={{ padding: "10px" }}>
              {qusetion["answers"] && qusetion["answers"].length > 0 &&
                qusetion["answers"].map((answerItem, index) => {
                  let { content, aid, isCorrect } = answerItem;
                  return qusetion["correctAnswers"] > 1 ? (
                    <MultiChoice
                      key={index}
                      isCorrect={isCorrect}
                      content={content}
                      qusetion={qusetion}
                      setQusetion={setQusetion}
                      answerId={aid}
                      examAnswers={examAnswers}
                      setExamAnswers={setExamAnswers}
                      currentIndex={currentQuestion}
                    />
                  ) : (
                    <SingleChoice
                      key={index}
                      isCorrect={isCorrect}
                      content={content}
                      qusetion={qusetion}
                      setQusetion={setQusetion}
                      answerId={aid}
                      examAnswers={examAnswers}
                      setExamAnswers={setExamAnswers}
                      currentIndex={currentQuestion}
                    />
                  );
                })}
            </section>
            <div>
              <button
                // className={`${
                //   examAnswers.length !== questions.length ? "disabled" : ""
                // }`}
                style={{ display: examAnswers.length !== questions.length ? "none" : "" }}
                disabled={examAnswers.length !== questions.length}
                onClick={() =>
                  submitExamAnswers(history, eid, examAnswers, setExamAnswers)
                }
              >
                Submit Exam
              </button>
            </div>
          </React.Fragment>
        )}
      </section>
    </main>
  );
};

export default Exam;
