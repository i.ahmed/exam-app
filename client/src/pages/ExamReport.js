import React, { useEffect, useRef, useState } from "react";
import { AiOutlinePrinter } from "react-icons/ai";
import Alert from "../components/Alert";
import { useGlobalContext } from "../context/context";
import USTLogo from "../img/ust.png";

const ExamReport = () => {
  const {
    exams,
    setExams,
    students,
    setStudents,
    getExams,
    getStudents,
    notify,
    setNotify,
    fetchExamReport,
  } = useGlobalContext();
  const examRefContainer = useRef();
  const studentRefContainer = useRef();
  const [reportData, setReportData] = useState([]);

  const handleSubmit = (e) => {
    e.preventDefault();
    const examId = examRefContainer.current.value;
    const studentId = studentRefContainer.current.value;
    fetchExamReport(studentId, examId, setReportData, notify, setNotify);
  };

  useEffect(() => {
    getExams(setExams, notify, setNotify);
    getStudents(setStudents, notify, setNotify);
    fetchExamReport(0, 0, setReportData, notify, setNotify);
  }, []);

  return (
    <main className="report">
      {notify.status !== "" && <Alert />}
      <p
        className={`${reportData && reportData.length === 0 ? "disabled-button" : ""}`}
        onClick={() => window.print()}
      >
        <AiOutlinePrinter />
      </p>
      <img src={USTLogo} />
      <form onSubmit={handleSubmit}>
        <div className="row">
          <div className="form-control">
            <select ref={examRefContainer}>
              <option value="0">Select Exam </option>
              {exams && exams.length > 0 &&
                exams.map((exam) => {
                  const { eid, subject } = exam;
                  return (
                    <option value={eid} key={eid}>
                      {subject}
                    </option>
                  );
                })}
            </select>
          </div>
          <div className="form-control">
            <select ref={studentRefContainer}>
              <option value="0">Select Student </option>
              {students && students.length > 0 &&
                students.map((student) => {
                  const { sid, username } = student;
                  return (
                    <option value={sid} key={sid}>
                      {username}
                    </option>
                  );
                })}
            </select>
          </div>
        </div>
        <button type="submit">Fetch</button>
      </form>
      <table>
        <thead>
          <tr>
            <th>#</th>
            <th>student</th>
            <th>average</th>
            <th>exam</th>
            <th>exam mark</th>
            <th>final mark</th>
            <th>Appreciation</th>
            <th>Points</th>
          </tr>
        </thead>
        <tbody>
          {reportData && reportData.length > 0
            ? reportData.map((data, index) => {
                const { sid, student, examName,schoolAvg,finalMark, appreciation, score, point } =
                  data;
                return (
                  <tr key={index}>
                    <td>{sid}</td>
                    <td>{student}</td>
                    <td>{schoolAvg}</td>
                    <td>{examName}</td>
                    <td>{score}</td>
                    <td>{finalMark}</td>
                    <td>{appreciation}</td>
                    <td>{point}</td>
                  </tr>
                );
              })
            : null}
        </tbody>
      </table>
    </main>
  );
};

export default ExamReport;
