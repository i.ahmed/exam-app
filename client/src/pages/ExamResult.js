import React, { useState, useEffect } from "react";
import { AiOutlinePrinter } from "react-icons/ai";
import Alert from "../components/Alert";
import { useGlobalContext } from "../context/context";
import USTLogo from "../img/ust.png";

const ExamResult = () => {
  const [results, setResults] = useState([]);
  const { getStudentResults, notify, setNotify, setTitle } = useGlobalContext();
  const student = JSON.parse(localStorage.getItem("student"));

  useEffect(() => {
    getStudentResults(student.sid, setResults, notify, setNotify);
    //setTitle("RESULTS");
  }, []);

  return (
    <main className="results">
      <p
        className={`${results.length === 0 ? "disabled-button" : ""}`}
        onClick={() => window.print()}
      >
        <AiOutlinePrinter />
      </p>
      {results && results.length > 0 ? (
        <React.Fragment>
          <img src={USTLogo} />
          <table>
            <thead>
              <tr>
                <th>#</th>
                <th>Exam</th>
                <th>Mark</th>
                <th>Appreciation</th>
                <th>Points</th>
              </tr>
            </thead>
            <tbody>
              {results.length > 0 &&
                results.map((result) => {
                  const { examId, examName, appreciation, finalMark, point } =
                    result;
                  return (
                    <tr key={examId}>
                      <td>{examId}</td>
                      <td>{examName}</td>
                      <td>{finalMark}</td>
                      <td>{appreciation}</td>
                      <td>{point}</td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </React.Fragment>
      ) : (
        <Alert />
      )}
    </main>
  );
};

export default ExamResult;
