import React, { useState, useEffect } from "react";
import ExamChart from "../components/ExamChart";
import ExamPassChart from "../components/ExamPassChart";
import SummaryWidgets from "../components/SummaryWidgets";
import { useGlobalContext } from "../context/context";
const Home = () => {
  const { getSummary } = useGlobalContext();
  const [userCount, setUserCount] = useState(0);
  const [studentCount, setStudentCount] = useState(0);
  const [examCount, setExamCount] = useState(0);
  const [questionCount, setQuestionCount] = useState(0);
  const [examSummary, setExamSummary] = useState({ labels: [], data: [] });
  const [examPassSummary, setExamPassSummary] = useState({
    labels: [],
    data: [],
  });

  useEffect(() => {
    getSummary(
      setUserCount,
      setStudentCount,
      setExamCount,
      setQuestionCount,
      setExamSummary,
      setExamPassSummary
    );
  }, []);

  return (
    <React.Fragment>
      <SummaryWidgets
        userCount={userCount}
        studentCount={studentCount}
        examCount={examCount}
        questionCount={questionCount}
      />
      <section
        style={{
          display: "grid",
          gridTemplateColumns: "repeat(auto-fit, minmax(400px, 1fr))",
          gap: 10,
          maxWidth: "100%",
        }}
      >
        <ExamChart examSummary={examSummary} />
        <ExamPassChart examPassSummary={examPassSummary} />
      </section>
    </React.Fragment>
  );
};

export default Home;
