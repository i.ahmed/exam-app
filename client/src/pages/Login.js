import React, { useEffect } from "react";
import { AiOutlineUser, AiOutlineLock } from "react-icons/ai";
import { useGlobalContext } from "../context/context";
import USTLogo from "../img/ust.png";
import "../index.css";
const Login = ({ history }) => {
  const { user, setUser, loginUser } = useGlobalContext();
  const handleSubmit = (e) => {
    e.preventDefault();
    if (user.username !== "" && user.password !== "") {
      loginUser(user, setUser);
    }
  };

  useEffect(() => {
    if (user.id !== 0) {
      history.replace("/dashboard");
    }
  }, [user, history])

  return (
    <div className="wrapper">
      <form className="auth" onSubmit={handleSubmit}>
        <img src={USTLogo} alt="ust logo" />
        <div className="form-control">
          <AiOutlineUser />
          <input
            type="text"
            name="username"
            value={user.username}
            placeholder="Enter your username"
            autoComplete="off"
            onChange={(e) => setUser({ ...user, username: e.target.value })}
            required
          />
        </div>
        <div className="form-control">
          <AiOutlineLock />
          <input
            type="password"
            name="password"
            placeholder="Enter your password"
            value={user.password}
            onChange={(e) => setUser({ ...user, password: e.target.value })}
            required
          />
        </div>
        <button type="submit">login</button>
      </form>
    </div>
  );
};
export default Login;
