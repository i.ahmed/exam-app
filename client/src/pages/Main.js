import React, { useState, useEffect } from "react";
import { BiLogOut } from "react-icons/bi";
import { BsBookmarks } from "react-icons/bs";
import { Link, useHistory } from "react-router-dom";
import Header from "../components/Header";
import { useGlobalContext } from "../context/context";
import StudentInfo from "../components/StudentInfo";

const Main = ({ children }) => {
  const [closeMenu, setCloseMenu] = useState(false);
  const [isOpend, setOpen] = useState(false);
  const { title, setTitle, logoutStudent, student, setStudent } =
    useGlobalContext();
  const history = useHistory();

  useEffect(() => {
    if (student.sid === 0) {
      history.replace("/");
    }
  }, [history, student]);

  useEffect(() => {
    setTitle("EXAMS");
  }, []);

  return (
    <React.Fragment>
      <Header closeMenu={closeMenu} setCloseMenu={setCloseMenu} title={title}>
        <nav style={{ display: "flex", alignItems: "center" }}>
          <span style={{ display: 'flex', alignItems: 'center', position: 'relative', cursor: 'pointer' }} onClick={() => setOpen(!isOpend)}>
            <StudentInfo isOpend={isOpend} />
          </span>
          {/* <Link
            to="/main/results"
            style={{
              display: "flex",
              alignItems: "center",
              margin: "0 20px",
              textTransform: "capitalize",
              textDecoration: "none",
              color: "#58555e",
            }}
            title="Your Results"
          >
            <BsBookmarks
              fontSize="1.3rem"
              style={{ cursor: "pointer", marginRight: "5px" }}
            />
          </Link> */}
          <span
            title="logout"
            onClick={() => logoutStudent(history, student, setStudent)}
          >
            <BiLogOut fontSize="1.5rem" style={{ cursor: "pointer" }} />
          </span>
        </nav>
      </Header>
      {children}
    </React.Fragment>
  );
};

export default Main;
