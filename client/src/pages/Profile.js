import React from "react";
import { AiOutlineUser, AiOutlineLock } from "react-icons/ai";
import { useGlobalContext } from "../context/context";
import Alert from "../components/Alert";

const Profile = () => {
  const { user, setUser, notify, setNotify, updateUser } = useGlobalContext();
  const handleSubmit = (e) => {
    e.preventDefault();
    if (user.username !== "") {
      updateUser(user, notify, setNotify);
    }
  };
  return (
    <main>
      {notify.status !== "" && <Alert />}
      <h2>Profile</h2>
      <form className="auth" onSubmit={handleSubmit}>
        <div className="form-control">
          <AiOutlineUser />
          <input
            type="text"
            name="username"
            value={user.username}
            placeholder="Enter your username"
            autoComplete="off"
            onChange={(e) => setUser({ ...user, username: e.target.value })}
            required
          />
        </div>
        <div className="form-control">
          <AiOutlineLock />
          <input
            type="password"
            name="password"
            placeholder="Enter new complex password"
            value={user.password}
            onChange={(e) => setUser({ ...user, password: e.target.value })}
          />
        </div>
        <button type="submit">update</button>
      </form>
    </main>
  );
};

export default Profile;
