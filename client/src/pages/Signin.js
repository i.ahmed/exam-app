import React, { useEffect } from "react";
import { AiOutlineUser, AiOutlineLock } from "react-icons/ai";
import { useHistory } from "react-router";
import { useGlobalContext } from "../context/context";
import USTLogo from "../img/ust.png";
import "../index.css";
const Signin = () => {
  const {
    student,
    setStudent,
    loginStudent,
    notify,
    setNotify,
    specs,
    setSpecs,
    getSpecs,
  } = useGlobalContext();
  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      student.username !== "" &&
      student.password !== "" &&
      student.specId !== 0
    ) {
      loginStudent(student, setStudent);
    }
  };

  useEffect(() => {
    if (student.sid !== 0) {
      history.replace("/main");
    }
  }, [student]);

  useEffect(() => {
    getSpecs(setSpecs,true, notify, setNotify);
  }, []);

  return (
    <div className="wrapper">
      <form className="auth" onSubmit={handleSubmit}>
        <img src={USTLogo} alt="ust logo" />
        <div className="form-control">
          <AiOutlineUser />
          <input
            type="text"
            name="username"
            value={student.username}
            placeholder="Enter your username"
            autoComplete="off"
            onChange={(e) =>
              setStudent({ ...student, username: e.target.value })
            }
            required
          />
        </div>
        <div className="form-control">
          <AiOutlineLock />
          <input
            type="password"
            name="password"
            placeholder="Enter your password"
            value={student.password}
            onChange={(e) =>
              setStudent({ ...student, password: e.target.value })
            }
            required
          />
        </div>
        <div className="form-control">
          <select
            style={{ width: "100%", outline: "none", border: "none" }}
            onChange={(e) =>
              setStudent({ ...student, specId: parseInt(e.target.value) })
            }
          >
            <option value="0"> Select Specialization </option>
            {specs && specs.length > 0 &&
              specs.map((sepc) => {
                const { spec_id, spec_name } = sepc;
                return (
                  <option value={spec_id} key={spec_id}>
                    {spec_name}
                  </option>
                );
              })}
          </select>
        </div>
        <button type="submit">login</button>
      </form>
    </div>
  );
};

export default Signin;
