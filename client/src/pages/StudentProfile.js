import React, { useEffect } from "react";
import { AiOutlineLock, AiOutlineUser } from "react-icons/ai";
import { useGlobalContext } from "../context/context";
import Alert from "../components/Alert";

const StudentProfile = () => {
  const { student, setStudent, notify, setNotify, setTitle, updateStudent } =
    useGlobalContext();
  const handleSubmit = (e) => {
    e.preventDefault();
    if (student.username === "") {
      return setNotify({
        ...notify,
        status: "error",
        message: "Username is required",
      });
    }
    updateStudent(student, notify, setNotify);
  };

  useEffect(() => {
    setTitle("PROFILE");
  }, []);

  return (
    <div className="wrapper" style={{ marginTop: "50px" }}>
      <form className="auth" onSubmit={handleSubmit}>
        {notify.status !== "" && <Alert />}
        <div className="form-control">
          <AiOutlineUser />
          <input
            type="text"
            name="username"
            value={student.username}
            placeholder="Enter your username"
            autoComplete="off"
            onChange={(e) =>
              setStudent({ ...student, username: e.target.value })
            }
            // required
          />
        </div>
        <div className="form-control">
          <AiOutlineLock />
          <input
            type="password"
            name="password"
            placeholder="Enter your password"
            value={student.password}
            onChange={(e) =>
              setStudent({ ...student, password: e.target.value })
            }
          />
        </div>
        <button type="submit">save</button>
      </form>
    </div>
  );
};

export default StudentProfile;
