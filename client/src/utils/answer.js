const addAnswer = (
  notify,
  setNotify,
  answer,
  setAnswer,
  qusetion,
  setQusetion
) => {
  if (answer.content === "") {
    return setNotify({
      ...notify,
      status: "error",
      message: "Answer field is required",
    });
  }

  if (answer.isCorrect) {
    let answers = qusetion.answers;
    answers.push(answer);
    setQusetion({ ...qusetion, answers });
    return setAnswer({ ...answer, content: "" });
  }

  let answers = qusetion.answers;
  answers.push(answer);
  setQusetion({ ...qusetion, answers });
  setAnswer({ ...answer, content: "" });
  setNotify({ ...notify, status: "", message: "" });
};
const deleteAnswer = (id, qusetion, setQusetion) => {
  const clearedAnswers = qusetion.answers.filter((answerItem, index) => {
    if (index !== id) return answerItem;
  });
  setQusetion({ ...qusetion, answers: clearedAnswers });
};

export { addAnswer, deleteAnswer };
