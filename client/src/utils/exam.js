import requests from "./request";

const getExams = async (setExams, notify, setNotify) => {
  const data = await requests(`/Backend/exams`, "GET");

  if (data && data.status === "error") {
    return setNotify({ ...notify, status: data.status, message: data.message });
  }

  setExams(data);
};

const getExam = async (id, exam, setExam, notify, setNotify) => {
  const data = await requests(`/Backend/exams?id=${id}`, "GET");
  if (data.status && data.status === "error") {
    return setNotify({ ...notify, status: data.status, message: data.message });
  }

  setExam({ ...exam, subject: data.subject, eid: data.eid });
};

const getUserNewExams = async (sid, setExams) => {
  const data = await requests(`/Backend/examlist?sid=${sid}`, "GET");
  if (!Object.keys(data).includes("status")) {
    return setExams(data);
  }

  setExams([]);
};

const fetchExamReport = async (sid, eid, setReportData, notify, setNotify) => {
  let route = `/Backend/reports/exam`;
  if (parseInt(sid) !== 0 && parseInt(eid) !== 0) {
    route = `/Backend/reports/exam?sid=${sid}&eid=${eid}`;
  } else if (parseInt(sid) !== 0) {
    route = `/Backend/reports/exam?sid=${sid}`;
  } else if (parseInt(eid) !== 0) {
    route = `/Backend/reports/exam?eid=${eid}`;
  }

  const data = await requests(route, "GET");
  if (Object.keys(data).includes("status")) {
    return setNotify({ ...notify, status: data.status, message: data.message });
  }

  setReportData(data);
};

const fetchStudentExamAnswers = async (
  sid,
  eid,
  setQuestions,
  notify,
  setNotify
) => {
  const data = await requests(
    `/Backend/reports/answers?sid=${sid}&eid=${eid}`,
    "POST"
  );

  if (Object.keys(data).includes("status") && data.status === "error") {
    return setNotify({ ...notify, status: data.status, message: data.message });
  }

  setQuestions(data);
};

const addExam = async (exam, setExam, notify, setNotify) => {
  const data = await requests(
    `/Backend/exams?subject=${exam.subject}&spec_id=${exam.spec_id}`,
    "POST"
  );

  if (Object.keys(data).includes("status")) {
    setNotify({ ...notify, status: data.status, message: data.message });
  }

  setExam({ ...exam, subject: "" });
};

const updateExam = async (exam, notify, setNotify) => {
  const data = await requests(
    `/Backend/exams?id=${exam.eid}&subject=${exam.subject}&spec_id=${exam.spec_id}`,
    "PUT"
  );

  setNotify({ ...notify, status: data.status, message: data.message });
};

const deleteExam = async (eid, notify, setNotify) => {
  const data = await requests(`/Backend/exams?id=${eid}`, "DELETE");
  setNotify({ ...notify, status: data.status, message: data.message });
};

const addExamAnswers = (
  questionId,
  answerId,
  examAnswers,
  setExamAnswers,
  isMultiChoice = false,
  checked = true
) => {
  let allExamAnswers = examAnswers;
  if (examAnswers.length === 0) {
    allExamAnswers.push({
      questionId,
      answer: [answerId],
    });
    localStorage.setItem("examAnswers", JSON.stringify(allExamAnswers));
    return setExamAnswers([...allExamAnswers]);
  }

  if (examAnswers.length > 0) {
    const matchedQuestion = allExamAnswers.find(
      (answer) => answer.questionId === questionId
    );

    if (!matchedQuestion) {
      allExamAnswers.push({
        questionId,
        answer: [answerId],
      });
      localStorage.setItem("examAnswers", JSON.stringify(allExamAnswers));
      return setExamAnswers([...allExamAnswers]);
    }
  }

  const currentIndex = findQuestionIndex(allExamAnswers, questionId);

  if (isMultiChoice) {
    if (!checked) {
      allExamAnswers[currentIndex]["answer"] = allExamAnswers[currentIndex][
        "answer"
      ].filter((answer) => answer !== answerId);

      if (allExamAnswers[currentIndex]["answer"].length === 0) {
        allExamAnswers = allExamAnswers.filter(
          (answer) => answer.questionId !== questionId
        );
      }

      localStorage.setItem("examAnswers", JSON.stringify(allExamAnswers));
      return setExamAnswers([...allExamAnswers]);
    }

    const matchedAnswer = allExamAnswers[currentIndex]["answer"].find(
      (answer) => answer === answerId
    );

    if (!matchedAnswer) {
      allExamAnswers[currentIndex]["answer"].push(answerId);
    }
  } else {
    allExamAnswers[currentIndex]["answer"] = [answerId];
  }

  localStorage.setItem("examAnswers", JSON.stringify(allExamAnswers));
  setExamAnswers([...allExamAnswers]);
};

const findQuestionIndex = (allExamAnswers, qid) =>
  allExamAnswers.findIndex((answer) => answer.questionId === qid);

const submitExamAnswers = async (history, examId, examAnswers) => {
  const student = JSON.parse(localStorage.getItem("student"));
  const data = await requests(
    `/Backend/results?eid=${examId}&sid=${student.sid}`,
    "POST",
    examAnswers,
    {
      "Content-Type": "application/json",
    }
  );
  if (data.status === "success") {
    localStorage.removeItem("examAnswers");
    localStorage.removeItem("questions");
    history.replace("/main");
  }
};

export {
  addExam,
  updateExam,
  addExamAnswers,
  getExams,
  getExam,
  getUserNewExams,
  fetchExamReport,
  fetchStudentExamAnswers,
  deleteExam,
  submitExamAnswers,
  findQuestionIndex,
};
