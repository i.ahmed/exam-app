import requests from "./request";

const getQuestion = async (
  examId,
  qid,
  notify,
  setNotify,
  qusetion,
  setQusetion
) => {
  const data = await requests(
    `/Backend/questions?eid=${examId}&qid=${qid}`,
    "GET"
  );

  if (Object.keys(data).includes("status")) {
    setNotify({ ...notify, status: data.status, message: data.message });
  } else {
    setQusetion({
      ...qusetion,
      eid: data.eid,
      qid: data.qid,
      qusetion: data.qusetion,
      answers: data.answers,
      correctAnswers: data.correctAnswers,
    });
  }
};

const getQuestions = async (
  examId,
  notify,
  setNotify,
  setQuestions,
  saveInLocalStorage = true
) => {
  const data = await requests(`/Backend/questions?eid=${examId}`, "GET");

  if (Object.keys(data).includes("status")) {
    return setNotify({ ...notify, status: data.status, message: data.message });
  }

  setQuestions(!data.length ? [data] : data);
  
  if (saveInLocalStorage)
    localStorage.setItem("questions", JSON.stringify(data));
};
const addQuestion = async (
  examId,
  qusetion,
  setQusetion,
  notify,
  setNotify
) => {
  const data = await requests(
    `/Backend/questions?eid=${examId}`,
    "POST",
    qusetion,
    {
      "Content-Type": "application/json",
    }
  );

  if (Object.keys(data).includes("status")) {
    setNotify({ ...notify, status: data.status, message: data.message });
  }

  setQusetion({ ...qusetion, qusetion: "", answers: [] });
};

const updateQuestion = async (examId, qusetion, notify, setNotify) => {
  const data = await requests(
    `/Backend/questions?eid=${examId}`,
    "PUT",
    qusetion,
    {
      "Content-Type": "application/json",
    }
  );
  setNotify({ ...notify, status: data.status, message: data.message });
};

const deleteQuestion = async (qid, notify, setNotify) => {
  const data = await requests(`/Backend/questions?id=${qid}`, "DELETE");
  setNotify({ ...notify, status: data.status, message: data.message });
};

export {
  getQuestion,
  getQuestions,
  addQuestion,
  updateQuestion,
  deleteQuestion,
};
