const requests = async (route, method, bodyData = null, headers = {}) => {
  let config = { method, cors: "no-cors" };
  if (headers) {
    config["headers"] = headers;
  }
  if (bodyData != null) {
    config["body"] = JSON.stringify(bodyData);
  }
  const response = await fetch(route, config);
  const data = response.state !== 404 ? await response.json() : {};
  return data;
};

export default requests;
