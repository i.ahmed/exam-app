import requests from "./request";

export const getSpecs = async (
  setSpecs,
  onlyActive,
  notify,
  setNotify
) => {
  const data = await requests(
    `/Backend/specs${onlyActive ? "?only_active=1": ''}`,
    "GET"
  );

  if (Object.keys(data).includes("status")) {
    return setNotify({ ...notify, status: data.status, message: data.message });
  }

  setSpecs(data);
};

export const getSpec = async (id, spec, setSpec, notify, setNotify) => {
  const data = await requests(`/Backend/specs?id=${id}`, "GET");

  if (Object.keys(data).includes("status")) {
    return setNotify({ ...notify, status: data.status, message: data.message });
  }

  setSpec({
    ...spec,
    spec_id: data.spec_id,
    spec_name: data.spec_name,
    is_active: data.isActive,
  });
};

export const addSpec = async (spec, setSpec, notify, setNotify) => {
  const data = await requests(
    `/Backend/specs?spec_name=${spec.spec_name}&is_active=${spec.is_active}`,
    "POST"
  );
  
  if (Object.keys(data).includes("status")) {
     setNotify({ ...notify, status: data.status, message: data.message });
  }

  setSpec({ ...spec, spec_name: "", is_active: true });
};

export const updateSpec = async (spec, notify, setNotify) => {
  const data = await requests(
    `/Backend/specs?id=${spec.spec_id}&spec_name=${spec.spec_name}&is_active=${spec.is_active}`,
    "PUT"
  );
  
  setNotify({ ...notify, status: data.status, message: data.message });
};
