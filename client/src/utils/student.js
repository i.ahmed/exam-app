import readXlsxFile from 'read-excel-file';
import requests from "../utils/request";

const loginStudent = async (student, setStudent) => {
  const data = await requests(
    `/Backend/auth?spec_id=${student.specId}&username=${student.username}&password=${student.password}`,
    "POST"
  );

  if (Object.keys(data).includes("status")) {
    return alert(data.message);
  }

  setStudent({ ...student, ...data });

  localStorage.setItem(
    "student",
    JSON.stringify({
      specId: data.specId,
      username: data.username,
      sid: data.sid,
      schoolAvg: data.schoolAvg,
      img: data.password
    })
  );
};

const logoutStudent = async (history, student, setStudent) => {
  localStorage.removeItem("student");
  setStudent({ ...student, sid: 0, username: "", password: "", spec_id: 0 });
  history.replace("/");
};

const getStudents = async (setStudents, notify, setNotify) => {
  const data = await requests(`/Backend/students`, "GET");

  if (Object.keys(data).includes("status")) {
    return setNotify({ ...notify, status: data.status, message: data.message });
  }

  setStudents(data);
};

const getStudent = async (id, student, setStudent, notify, setNotify) => {
  const data = await requests(`/Backend/students?id=${id}`, "GET");

  if (Object.keys(data).includes("status")) {
    return setNotify({ ...notify, status: data.status, message: data.message });
  }

  setStudent({
    ...student,
    sid: data.sid,
    specId: data.specId,
    username: data.username,
    schoolAvg: data.schoolAvg
  });
};

const addStudent = async (student, specName = '',showNotify = true, notify, setNotify) => {
  let requestUrl = `/Backend/students?spec_id=${student.specId}&username=${student.username}&password=${student.password}&school_avg=${student.schoolAvg}`;

  if (specName !== '') {
    requestUrl += `&spec_name=${specName}`;
  }

  const data = await requests(requestUrl, "POST");

  if(showNotify) {
    setNotify({ ...notify, status: data.status, message: data.message });
  }

  return true;
};

const updateStudent = async (student, notify, setNotify) => {
  let route = "";
  if (student.password !== "") {
    route = `/Backend/students?spec_id=${student.specId}&id=${student.sid}&username=${student.username}&password=${student.password}&school_avg=${student.schoolAvg}`;
  } else {
    route = `/Backend/students?spec_id=${student.specId}&id=${student.sid}&username=${student.username}&school_avg=${student.schoolAvg}`;
  }

  const data = await requests(route, "PUT");

  setNotify({ ...notify, status: data.status, message: data.message });
};

const deleteStudent = async (id, notify, setNotify, setRefresh) => {
  const data = await requests(`/Backend/students?id=${id}`, "DELETE");

  setNotify({ ...notify, status: data.status, message: data.message });

  if (data && data.status === "success") {
    setRefresh(true);
  }
};

const getStudentResults = async (sid, setResults, notify, setNotify) => {
  const data = await requests(`/Backend/results?sid=${sid}`, "GET");

  if (!Object.keys(data).includes("status")) {
    return setResults(data);
  }

  setNotify({ ...notify, status: data.status, message: data.message });
};


const uploadStudents = async (file, notify, setNotify) => {
  try {
    const rows = await readXlsxFile(file);

    console.log(rows);
    for (let index = 1; index < rows.length; index++) {
      const [password, username, schoolAvg, specName] = rows[index];

      console.log("--------------", password);

      const response = await addStudent({specId: 0, username,password,schoolAvg}, specName, false, null,null);
    }

    setNotify({ ...notify, status: 'success', message: 'Students uploaded succssfully' })
  } catch (error) {
    console.log(error);
  }
}

export {
  loginStudent,
  logoutStudent,
  getStudents,
  getStudent,
  addStudent,
  updateStudent,
  deleteStudent,
  getStudentResults,
  uploadStudents
};
