import requests from "./request";
import uniqolor from "uniqolor";

const getSummary = async (
  setUserCount,
  setStudentCount,
  setExamCount,
  setQuestionCount,
  setExamSummary,
  setExamPassSummary
) => {
  const data = await requests(`/Backend/summary`, "GET");
  if (Object.keys(data).includes("status")) {
    return alert(data.message);
  }
 
  setUserCount(data.statistics.user_count);
  setStudentCount(data.statistics.student_count);
  setExamCount(data.statistics.exam_count);
  setQuestionCount(data.statistics.question_count);
  // exam student count summary chart
  let summary = { labels: [], data: [], colors: [] };
  data.examStudentCountSummary.forEach((summaryItem) => {
    const [key] = Object.keys(summaryItem);
    summary.labels.push(key);
    summary.data.push(summaryItem[key]);
    const { color } = uniqolor.random();
    summary.colors.push(color);
  });
  setExamSummary(summary);
  //exam student pass count summary count
  summary = { labels: [], data: [], colors: [] };
  data.examStudentPassCountSummary.forEach((summaryItem) => {
    const [key] = Object.keys(summaryItem);
    summary.labels.push(key);
    summary.data.push(summaryItem[key]);
    const { color } = uniqolor.random();
    summary.colors.push(color);
  });
  setExamPassSummary(summary);
};

export { getSummary };
