import requests from "./request";

const loginUser = async (user, setUser) => {
  const data = await requests(
    `/Backend/users?username=${user.username}&password=${user.password}`,
    "POST"
  );
  
  if (Object.keys(data).includes("status")) {
    return alert(data.message);
  }
  
  setUser({ ...user, ...data });
  
  localStorage.setItem(
    "user",
    JSON.stringify({ username: data.username, id: data.id })
  );
};

const logoutUser = async (history, user, setUser) => {
  localStorage.removeItem("user");
  setUser({ ...user, id: 0, username: "", password: "" });
  history.replace("/login");
};

const updateUser = async (user, notify, setNotify) => {
  const data = await requests(
    `/Backend/users?id=${user.id}&username=${user.username}&password=${user.password}`,
    "PUT"
  );
  
  if (data.status === "error") {
    return setNotify({ ...notify, status: data.status, message: data.message });
  }

  localStorage.setItem(
    "user",
    JSON.stringify({ username: user.username, id: user.id })
  );
};

export { loginUser, logoutUser, updateUser };
